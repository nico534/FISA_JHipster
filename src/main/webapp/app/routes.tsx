import React from 'react';
import { Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import Logout from 'app/modules/login/logout';
import Home from 'app/modules/home/home';
import PrivateRoute from 'app/shared/auth/private-route';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import PageNotFound from 'app/shared/error/page-not-found';
import { AUTHORITIES } from 'app/config/constants';
import ProjectPageLoader from 'app/modules/projectPage/ProjectPageLoader';
import * as RoutValues from 'app/variables/routes';

const Admin = Loadable({
  loader: () => import(/* webpackChunkName: "administration" */ 'app/modules/administration'),
  loading: () => <div>loading ...</div>,
});

const Routes = () => (
  <div className="view-routes">
    <Switch>
      <ErrorBoundaryRoute path={RoutValues.LOGOUT} component={Logout} />
      <PrivateRoute path={RoutValues.ADMIN} component={Admin} hasAnyAuthorities={[AUTHORITIES.ADMIN]} />
      <PrivateRoute path={RoutValues.PROJECT} exact component={ProjectPageLoader} hasAnyAuthorities={[AUTHORITIES.USER]} />
      <ErrorBoundaryRoute path={RoutValues.HOME} exact component={Home} />
      <ErrorBoundaryRoute component={PageNotFound} />
    </Switch>
  </div>
);

export default Routes;
