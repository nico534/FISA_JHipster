import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button, Grid, Typography, Paper, Snackbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MuiAlert from '@material-ui/lab/Alert';
import { loadAutoSave, fetchAvailableProjects, clearErrorMessage, resetState } from 'app/shared/actions';
import { ErrorMessageI } from 'app/shared/model';

import {
  ErrorSnackbar,
  DeveloperMenu,
  LoadExternalSaved,
  SavedProjectsOverview,
} from 'app/modules/components';
import { getServerCommunicationError, getProjectsFetched, getAvailableProjects, getNotSaved } from 'app/shared/selectors';
import { Translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import ErrorBoundary from 'app/shared/error/error-boundary';
import Header from 'app/shared/layout/header/header';

const useStyles = makeStyles(theme => ({
  background: {
    height: '100vh ',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.default,
  },
  selectionPaper: {
    width: '60%',
    textAlign: 'center',
    margin: '2rem',
    padding: 20,
  },
  projectsList: {
    width: '60%',
    margin: '0 auto',
  },
  projectsListItems: {
    textAlign: 'center',
  },
  addProjectButton: {
    width: '2rem',
    height: '2rem',
    transform: 'scale(1.5)',
  },
  heading: {
    marginBottom: '2rem',
    fontWeight: 'bolder',
  },
  spaceHeading: {
    marginBottom: '1rem',
    fontWeight: 'bolder',
    paddingTop: '2rem',
  },
  scrollArea: {
    minHeight: 'calc(100vh - 92px)',
    height: '100%',
    overflow: 'auto',
  },
}));

interface MainPageProps {
  fetchedAvailableProjects: boolean;
  fetchTheAvailableProjects: () => void;
  loadAutoSave: () => void;
  fetchAvailableProjectsError: ErrorMessageI | undefined;
  clearErrorMessage: () => void;
  resetProjectState: () => void;
  unsavedDataInAutosave: boolean;
}

function MainPage(props: MainPageProps) {
  const classes = useStyles();

  const [tryToFetch, setTryToFetch] = React.useState(true);
  const [developerMenuOpen, setDeveloperMenuOpen] = React.useState(false);
  const [loadAutosaveInfoOpen, setLoadAutosaveInfoOpen] = React.useState(props.unsavedDataInAutosave);
  const [warningOpen, setWarningOpen] = React.useState(false);

  const { resetProjectState, fetchTheAvailableProjects, fetchedAvailableProjects, fetchAvailableProjectsError } = props;

  useEffect(() => {
    if (tryToFetch && !fetchedAvailableProjects) {
      fetchTheAvailableProjects();
      setTryToFetch(false);
      setTimeout(() => setTryToFetch(true), 2000);
    }
  }, [fetchedAvailableProjects, fetchTheAvailableProjects, tryToFetch]);

  useEffect(() => {
    resetProjectState();
  }, [resetProjectState]);

  useEffect(() => {
    if (fetchAvailableProjectsError) {
      setLoadAutosaveInfoOpen(false);
      setWarningOpen(true);
    }
  }, [fetchAvailableProjectsError]);

  return (
    <Paper className={classes.background} square>
      <div className={classes.scrollArea}>
        <Grid container alignItems="center" justify="space-around" direction="column" alignContent="space-around">
          <Paper className={classes.selectionPaper}>
            <Typography id="projectHeading" className={classes.heading} variant="h4">
              <Translate contentKey="mainPage.loadProjectHeading">Create or Load a Project</Translate>
            </Typography>
            <LoadExternalSaved />
            <Typography className={classes.spaceHeading} variant="h5">
              <Translate contentKey="mainPage.projectHeading">Projects on FISA:</Translate>
            </Typography>
            <SavedProjectsOverview />
          </Paper>
          <Paper className={classes.selectionPaper}>
            <Typography className={classes.heading} variant="h4">
              <Translate contentKey="mainPage.useCaseHeading">Manage Use-Cases</Translate>
            </Typography>
            <Button onClick={() => setDeveloperMenuOpen(true)} variant="outlined" color="primary">
              <Translate contentKey="mainPage.developerPageCta">Open developer-menu</Translate>
            </Button>
          </Paper>
        </Grid>
      </div>
      <DeveloperMenu open={developerMenuOpen} onClose={() => setDeveloperMenuOpen(false)} />
      <Snackbar open={loadAutosaveInfoOpen}>
        <MuiAlert variant="filled" severity="info" onClose={() => setLoadAutosaveInfoOpen(false)}>
          <Translate contentKey="mainPage.unsavedDataInfo">There is unsaved data, do you want to restore it?</Translate>
          <Button color="inherit" onClick={() => props.loadAutoSave()} style={{ marginLeft: 20 }} variant="outlined">
            <Link style={{ textDecoration: 'none', color: 'inherit' }} to="/project">
              <Translate contentKey="mainPage.restore">Restore</Translate>
            </Link>
          </Button>
        </MuiAlert>
      </Snackbar>
      <ErrorSnackbar
        open={warningOpen}
        error={props.fetchAvailableProjectsError}
        onClose={() => {
          props.clearErrorMessage();
          setWarningOpen(false);
        }}
      />
    </Paper>
  );
}

const mapStateToProps = (state: IRootState) => ({
  availableProjects: getAvailableProjects(state),
  fetchedAvailableProjects: getProjectsFetched(state),
  fetchAvailableProjectsError: getServerCommunicationError(state),
  unsavedDataInAutosave: getNotSaved(state),
});

const mapDispatchToProps = {
  fetchTheAvailableProjects: fetchAvailableProjects,
  loadAutoSave,
  clearErrorMessage,
  resetProjectState: resetState,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
