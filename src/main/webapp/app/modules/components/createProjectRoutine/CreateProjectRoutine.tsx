import React from 'react';
import { Dialog, Button, TextField, DialogContent, CircularProgress, DialogTitle, DialogActions, makeStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Routes } from 'app/shared/util/url-utils';
import { setFetchProjectName, fetchProject, setChosenDocumentId } from 'app/shared/actions';
import { getChosenDocumentId, getServerCommunicationPending, getServerCommunicationError } from 'app/shared/selectors';
import { ErrorMessageI } from 'app/shared/model';
import { Translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import ErrorDialogContent from '../errorMessages/ErrorDialogContent';
import UseCaseOverview from '../useCasesOverview/useCaseOverview';

enum CreateProjectStates {
  CHOOSE_USE_CASE,
  ENTER_NAME,
  PENDING,
}

interface CreateProjectRoutineProps {
  open: boolean;
  close: () => void;
  setFetchProjectName: (name: string) => void;
  fetchProject: (id: number) => void;
  chosenDocumentId: number | undefined;
  fetchPending: boolean;
  fetchError: ErrorMessageI | undefined;
}

interface CoosDefinitionProps {
  cancel: () => void;
  next: () => void;
  setChosenDocumentId: (id: number) => void;
}

function ChooseDefinitionRaw(props: CoosDefinitionProps) {
  const [selected, setSelected] = React.useState(0);
  return (
    <>
      <DialogTitle>
        <Translate contentKey="global.menu.createProject.chooseUsecase">Choose a Use-Case</Translate>
      </DialogTitle>
      <DialogContent>
        <UseCaseOverview selected={selected} setSelected={setSelected} removable={false} />
      </DialogContent>
      <DialogActions>
        <Button type="reset" onClick={props.cancel}>
          <Translate contentKey="global.menu.cancel">Cancel</Translate>
        </Button>
        <Button
          disabled={selected === 0}
          type="submit"
          onClick={() => {
            props.setChosenDocumentId(selected);
            props.next();
          }}
        >
          <Translate contentKey="global.menu.next">Next</Translate>
        </Button>
      </DialogActions>
    </>
  );
}

const ChooseDefinition = connect(null, { setChosenDocumentId })(ChooseDefinitionRaw);

interface AddProjectNameProps {
  back: () => void;
  submit: (name: string) => void;
}

const addProjectNameStyle = makeStyles(() => ({
  root: {
    width: 300,
  },
  nameForm: {
    textAlign: 'center',
    marginBottom: 5,
  },
}));

function AddProjectName(props: AddProjectNameProps) {
  const [name, setName] = React.useState('');

  const classes = addProjectNameStyle();

  return (
    <div className={classes.root}>
      <DialogTitle id="enterNameTitle">
        <Translate contentKey="global.menu.createProject.pleaseEnterProjectName">Please Enter the Project Name</Translate>
      </DialogTitle>
      <DialogContent>
        <form className={classes.nameForm}>
          <TextField
            autoFocus={true}
            onKeyDown={e => {
              switch (e.key) {
                case 'Enter':
                  e.stopPropagation();
                  if (name !== '') {
                    props.submit(name);
                  }
                  break;
                case 'Escape':
                  props.back();
                  break;
                default:
              }
            }}
            id="standard-basic"
            label="Project name"
            value={name}
            onChange={event => {
              setName(event.target.value);
            }}
          />
        </form>
      </DialogContent>
      <DialogActions>
        <Button type="reset" onClick={props.back}>
          <Translate contentKey="global.menu.back">Back</Translate>
        </Button>
        <Button type="submit" disabled={name === ''} onClick={() => props.submit(name)}>
          <Translate contentKey="global.menu.create">Create</Translate>
        </Button>
      </DialogActions>
    </div>
  );
}

function CreateProjectRoutine(props: CreateProjectRoutineProps) {
  const history = useHistory();

  const [routineState, setRoutineState] = React.useState(CreateProjectStates.CHOOSE_USE_CASE);

  const closeRoutine = () => {
    props.close();
    setTimeout(() => setRoutineState(CreateProjectStates.CHOOSE_USE_CASE), 500);
  };

  const createProject = (name: string) => {
    setRoutineState(CreateProjectStates.PENDING);
    props.setFetchProjectName(name);

    if (props.chosenDocumentId) {
      props.fetchProject(props.chosenDocumentId);
    }
  };

  let content;
  switch (routineState) {
    case CreateProjectStates.CHOOSE_USE_CASE:
      content = <ChooseDefinition cancel={closeRoutine} next={() => setRoutineState(CreateProjectStates.ENTER_NAME)} />;
      break;
    case CreateProjectStates.ENTER_NAME:
      content = <AddProjectName back={() => setRoutineState(CreateProjectStates.CHOOSE_USE_CASE)} submit={name => createProject(name)} />;
      break;
    case CreateProjectStates.PENDING:
      if (props.fetchPending) {
        content = (
          <DialogContent style={{ textAlign: 'center' }}>
            <CircularProgress />
          </DialogContent>
        );
      } else if (props.fetchError !== undefined) {
        content = <ErrorDialogContent error={props.fetchError} />;
      } else {
        history.push(Routes.PROJECT);
        content = <div />;
      }
      break;
    default:
      content = <div>WrongState</div>;
  }

  return <Dialog open={props.open}>{content}</Dialog>;
}

const mapStateToProps = (state: IRootState) => ({
  chosenDocumentId: getChosenDocumentId(state),
  fetchPending: getServerCommunicationPending(state),
  fetchError: getServerCommunicationError(state),
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const mapDispatchToProps = {
  setFetchProjectName,
  fetchProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateProjectRoutine);
