import React from 'react';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
  makeStyles,
  FormControlLabel,
  Checkbox,
  CircularProgress,
  Tooltip,
} from '@material-ui/core';
import { connect } from 'react-redux';
import { FisaProjectI, ErrorMessageI } from 'app/shared/model';
import { uploadProjectToFrost, clearErrorMessage, updateProjectOnFrost } from 'app/shared/actions';
import {
  getServerCommunicationPending,
  getServerCommunicationError,
  getServerCommunicationActive,
  getFisaProjectFromState,
  getConnectedFrostServer,
} from 'app/shared/selectors';
import { IRootState } from 'app/shared/reducers';
import { Translate } from 'react-jhipster';
import Success from './UploadSuccessMessage';
import ErrorDialogContent from '../errorMessages/ErrorDialogContent';
import {DEFAULT_FROST_SERVER_URL} from "app/config/constants";

const uploadDialogStyle = makeStyles(() => ({
  uploadDialRoot: {
    minWidth: 750,
    minHeight: 400,
  },
}));

export interface UploadToFrostDialogProps {
  open: boolean;
  handleClose: () => void;
  getFisaProject: (withExampleData: boolean, ignoreFrostIds: boolean) => FisaProjectI;
  communicationActive: boolean;
  communicationPending: boolean;
  communicationError: ErrorMessageI | undefined;
  clearErrorMessage: () => void;
  uploadProjectToFrost: (project: FisaProjectI, frostUrl: string) => void;
  updateProjectOnFrost: (project: FisaProjectI) => void;
  connectedFrostServerURL: string | undefined;
}

enum UploadStates {
  ENTER_PROPERTIES,
  PENDING,
  UPLOAD_WARNING,
}

function UploadToFrostDialog(props: UploadToFrostDialogProps) {
  const classes = uploadDialogStyle();
  const [currentUploadState, setCurrentUploadState] = React.useState(UploadStates.ENTER_PROPERTIES);
  const [urlTextDisabled, setUrlTextDisabled] = React.useState<boolean>(true);
  const [generateExampleDataSave, setGenerateExampleDataSave] = React.useState(false);

  const [url, setUrl] = React.useState(props.connectedFrostServerURL || DEFAULT_FROST_SERVER_URL);

  const reset = () => {
    setCurrentUploadState(UploadStates.ENTER_PROPERTIES);
    props.clearErrorMessage();
  };

  const onClose = () => {
    props.handleClose();
    setTimeout(() => setCurrentUploadState(UploadStates.ENTER_PROPERTIES), 250);
  };

  const upload = (generateExampleData: boolean, ignoreFrostIds: boolean) => {
    if (ignoreFrostIds && props.connectedFrostServerURL && currentUploadState !== UploadStates.UPLOAD_WARNING) {
      setCurrentUploadState(UploadStates.UPLOAD_WARNING);
      setGenerateExampleDataSave(generateExampleData);
      setUrlTextDisabled(true);
      return;
    }
    setCurrentUploadState(UploadStates.PENDING);

    // Update on FROST
    if (props.connectedFrostServerURL && !ignoreFrostIds && urlTextDisabled) {
      props.updateProjectOnFrost(props.getFisaProject(generateExampleData, ignoreFrostIds));
    } else {
      // Upload to frost
      props.uploadProjectToFrost(props.getFisaProject(generateExampleData, ignoreFrostIds), url);
    }
  };

  let content;
  switch (currentUploadState) {
    case UploadStates.ENTER_PROPERTIES:
      content = (
        <UploadDialog
          urlDisabled={urlTextDisabled}
          setUrlDisabled={setUrlTextDisabled}
          url={url}
          setUrl={setUrl}
          {...props}
          upload={upload}
        />
      );
      break;

    case UploadStates.UPLOAD_WARNING:
      content = (
        <UploadWarning
          cancel={() => {
            reset();
          }}
          upload={() => upload(generateExampleDataSave, true)}
        />
      );
      break;
    case UploadStates.PENDING:
      if (props.communicationPending) {
        content = <Pending />;
      } else if (props.communicationError) {
        content = (
          <Error
            error={props.communicationError}
            close={() => {
              reset();
              onClose();
            }}
            resetState={() => reset()}
          />
        );
      } else {
        content = <Success frostUrl={url} close={() => onClose()} />;
      }

      break;
    default:
      content = <div>No more states</div>;
  }
  return (
    <Dialog maxWidth="lg" open={props.open} aria-labelledby="form-dialog-title" className={classes.uploadDialRoot}>
      {content}
    </Dialog>
  );
}

interface UploadDialogProps extends UploadToFrostDialogProps {
  upload: (generateExampleData: boolean, ignoreFrostIds: boolean) => void;
  url: string;
  setUrl: (url: string) => void;
  urlDisabled: boolean;
  setUrlDisabled: (disabled: boolean) => void;
}

function UploadDialog(props: UploadDialogProps) {
  const [withExampleData, setWithExampleData] = React.useState(false);
  const handleClose = () => {
    props.handleClose();
  };

  const handleUpload = () => {
    props.upload(withExampleData, !(props.urlDisabled && !!props.connectedFrostServerURL));
  };
  return (
    <>
      <DialogTitle id="form-dialog-title">
        <Translate contentKey="global.menu.uploadToFrost.toFrostTooltip">Upload to Frost-Server</Translate>
      </DialogTitle>
      <DialogContent>
        {(!props.urlDisabled || !props.connectedFrostServerURL) && (
          <DialogContentText>
            <Translate contentKey="global.menu.uploadToFrost.enterFrostUrl">Please enter the Frost-Server URL</Translate>
          </DialogContentText>
        )}
        <TextField
          disabled={props.urlDisabled && !!props.connectedFrostServerURL}
          autoFocus
          value={props.url}
          onChange={e => props.setUrl(e.target.value)}
          margin="dense"
          id="name"
          label="Frost-Server URL"
          type="url"
          fullWidth
        />
        {(!props.urlDisabled || !props.connectedFrostServerURL) && (
          <Tooltip
            title={
              <Translate contentKey="global.menu.uploadToFrost.createExampleDataHelp">
                If this option is checked, FISA will generate some example Data
              </Translate>
            }
          >
            <FormControlLabel
              style={{ marginRight: 'auto' }}
              control={<Checkbox checked={withExampleData} onChange={() => setWithExampleData(oldData => !oldData)} />}
              label={<Translate contentKey="global.menu.uploadToFrost.createExampleData">Generate Example Data</Translate>}
            />
          </Tooltip>
        )}
        {props.connectedFrostServerURL && (
          <>
            <br />
            <Tooltip
              title={
                <Translate contentKey="global.menu.uploadToFrost.uploadToDifferentURLTooltip">
                  Upload this Project to a different FROST-Server
                </Translate>
              }
            >
              <FormControlLabel
                style={{ marginRight: 'auto' }}
                control={<Checkbox checked={!props.urlDisabled} onChange={() => props.setUrlDisabled(!props.urlDisabled)} />}
                label={<Translate contentKey="global.menu.uploadToFrost.uploadToDifferentURLLabel">Upload to different Server</Translate>}
              />
            </Tooltip>
          </>
        )}
      </DialogContent>
      <DialogActions>
        <Button type="reset" onClick={handleClose} color="primary">
          <Translate contentKey="global.menu.cancel">Cancel</Translate>
        </Button>
        <Button type="submit" onClick={handleUpload} color="primary">
          {props.urlDisabled && !!props.connectedFrostServerURL ? (
            <Translate contentKey="global.menu.update">Update</Translate>
          ) : (
            <Translate contentKey="global.menu.upload">Upload</Translate>
          )}
        </Button>
      </DialogActions>
    </>
  );
}

function Pending() {
  return (
    <>
      <DialogContent style={{ textAlign: 'center' }}>
        <CircularProgress />
      </DialogContent>
    </>
  );
}

interface UploadWarningProps {
  upload: () => void;
  cancel: () => void;
}

function UploadWarning(props: UploadWarningProps) {
  return (
    <>
      <DialogTitle id="form-dialog-title">
        <Translate contentKey="global.menu.uploadToFrost.uploadWarningTitle">Warning!</Translate>
      </DialogTitle>
      <DialogContent>
        <Translate contentKey="global.menu.uploadToFrost.uploadWarning">
          If you upload this project to a different FROST-Server the Entity-connections with the old Server will be lost!
        </Translate>
      </DialogContent>
      <DialogActions>
        <Button type="reset" onClick={() => props.cancel()} color="primary">
          <Translate contentKey="global.menu.back">Back</Translate>
        </Button>
        <Button type="submit" onClick={() => props.upload()} color="primary">
          <Translate contentKey="global.menu.upload">Upload</Translate>
        </Button>
      </DialogActions>
    </>
  );
}

interface ErrorProps {
  error: ErrorMessageI;
  close: () => void;
  resetState: () => void;
}

function Error(props: ErrorProps) {
  return (
    <>
      <ErrorDialogContent error={props.error} />
      <DialogActions>
        <Button onClick={props.close}>
          <Translate contentKey="global.menu.close">Close</Translate>
        </Button>
        <Button onClick={props.resetState}>
          <Translate contentKey="global.menu.tryAgain">Try again</Translate>
        </Button>
      </DialogActions>
    </>
  );
}

const stateToProps = (state: IRootState) => ({
  getFisaProject: (withExampleData: boolean, ignoreFrostIds: boolean) => getFisaProjectFromState(state, withExampleData, ignoreFrostIds),
  communicationActive: getServerCommunicationActive(state),
  communicationPending: getServerCommunicationPending(state),
  communicationError: getServerCommunicationError(state),
  connectedFrostServerURL: getConnectedFrostServer(state),
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const mapDispatchToProps = {
  clearErrorMessage,
  uploadProjectToFrost,
  updateProjectOnFrost,
};

export default connect(stateToProps, mapDispatchToProps)(UploadToFrostDialog);
