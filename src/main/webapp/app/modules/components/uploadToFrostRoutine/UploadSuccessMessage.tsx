/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import { connect } from 'react-redux';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MuiAlert from '@material-ui/lab/Alert';
import {
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  Link,
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
  makeStyles,
  Snackbar,
} from '@material-ui/core';
import { DatastreamData } from 'app/shared/model';
import { getDatastremConnectionData } from 'app/shared/selectors';
import { IRootState } from 'app/shared/reducers';
import { Translate } from 'react-jhipster';

const OGCInfoAddress = 'http://developers.sensorup.com/docs/#observations';

const styles = makeStyles(() => ({
  link: {
    textDecoration: 'none',
  },
  copyButton: {
    float: 'right',
  },
  listBox: {
    position: 'relative',
    overflow: 'auto',
    height: 250,
    width: 400,
  },
  projectsListItems: {
    textAlign: 'center',
  },
  topBox: {
    paddingBottom: 20,
  },
}));

interface SuccessProps {
  close: () => void;
  connectionData: DatastreamData[] | undefined;
  frostUrl: string;
}

function createCurlCommand(datastreamId: string | number, frostUrl: string) {
  const data = JSON.stringify(
    {
      phenomenonTime: '2017-02-07T18:02:00.000Z',
      resultTime: '2017-02-07T18:02:00.000Z',
      result: 21.6,
      Datastream: { '@iot.id': datastreamId },
    },
    null,
    2
  );
  return `curl -X POST -H "Content-Type: application/json" -d '${data}' "${frostUrl}/Observations"`;
}

function Success(props: SuccessProps) {
  const classes = styles();
  const [copySuccessOpen, setCopySuccessOpen] = React.useState(false);

  const openFrostServer = (url: string) => {
    window.open(url, '_blank');
  };

  return (
    <>
      <DialogTitle>
        <Translate contentKey="notification.uploadSuccess.successfullyUploaded">Successfully uploaded!</Translate>
      </DialogTitle>
      <DialogContent style={{ width: '100%', height: '100%' }}>
        <DialogContentText variant="h5">
          <Translate contentKey="notification.uploadSuccess.createdDatastreams">Created Datastreams:</Translate>
        </DialogContentText>
        {props.connectionData?.map((data, index) => {
          return (
            <Accordion key={data.name}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-label="Expand"
                aria-controls="additional-actions2-content"
                id="additional-actions2-header"
              >
                <a>
                  <Link
                    variant="h6"
                    onClick={(e) => {
                      openFrostServer(`${props.frostUrl}/Datastreams(${data.id})`);
                      e.stopPropagation();
                    }}
                  >
                    `${index + 1}`) &quot;{data.name}&quot;: {`{ @iot.id: ${data.id} }`}
                  </Link>
                </a>
              </AccordionSummary>
              <AccordionDetails>
                <div style={{ width: '100%' }}>
                  <Button
                    variant="outlined"
                    size="small"
                    className={classes.copyButton}
                    onClick={() => {
                      navigator.clipboard.writeText(createCurlCommand(data.id, props.frostUrl)).then(() => setCopySuccessOpen(true));
                    }}
                    color="primary"
                  >
                    <Translate contentKey="notification.uploadSuccess.copy">Copy</Translate>
                  </Button>
                  <Typography>
                    <Translate contentKey="notification.uploadSuccess.curlExample">Example access with Curl:</Translate>
                  </Typography>

                  <pre>{createCurlCommand(data.id, props.frostUrl)}</pre>
                </div>
              </AccordionDetails>
            </Accordion>
          );
        })}
        <DialogContentText variant="subtitle1">
          <Translate contentKey="notification.uploadSuccess.moreObservationInfo">
            For more information on how to work with OGC-SensorThings-API visit:
          </Translate>
          <br />

          <a href={OGCInfoAddress} target="_blank" rel="noopener">
            <Link>{OGCInfoAddress}</Link>
          </a>
        </DialogContentText>
      </DialogContent>

      <DialogActions>
        <Button onClick={props.close}>
          <Translate contentKey="global.menu.close">Close</Translate>
        </Button>
      </DialogActions>
      <Snackbar open={copySuccessOpen} autoHideDuration={2000} onClose={() => setCopySuccessOpen(false)}>
        <MuiAlert elevation={6} variant="filled" onClose={() => setCopySuccessOpen(false)} severity="success">
          <Translate contentKey="notification.uploadSuccess.copiedMsg">Copied to Clipboard!</Translate>
        </MuiAlert>
      </Snackbar>
    </>
  );
}

const successStateToProps = (state: IRootState) => ({
  connectionData: getDatastremConnectionData(state),
});

export default connect(successStateToProps)(Success);
