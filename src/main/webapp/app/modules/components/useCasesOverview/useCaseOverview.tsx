/* eslint-disable react/require-default-props */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import DeleteIcon from '@material-ui/icons/Delete';
import DownloadIcon from '@material-ui/icons/GetApp';
import { List, ListItem, ListItemText, IconButton, makeStyles, TextField, Box } from '@material-ui/core';
import { AvailableFisaDocumentI, ErrorMessageI } from 'app/shared/model';
import {
  getAvailableFisaDocuments,
  getServerCommunicationPending,
  getServerCommunicationError,
  getDocumentsFetched,
} from 'app/shared/selectors';
import { fetchAvailableDocuments, clearErrorMessage, deleteDocument } from 'app/shared/actions';
import { IRootState } from 'app/shared/reducers';
import { Translate } from 'react-jhipster';
import { BackendUrl } from 'app/shared/util/url-utils';
import RemoveWarning from '../errorMessages/RemoveWarning';
import ErrorSnackbar from '../errorMessages/ErrorSnackbar';

const styles = makeStyles(theme => ({
  root: {
    minWidth: 300,
    maxWidth: 450,
    backgroundColor: theme.palette.background.paper,
    padding: 0,
  },
  searchBox: {
    display: 'flex',
    justifyContent: 'left',
    alignItems: 'left',
    paddingBottom: 5,
  },
  listBox: {
    position: 'relative',
    overflow: 'auto',
    height: 250,
    width: 400,
  },
  projectsListItems: {
    textAlign: 'center',
  },
  topBox: {
    paddingBottom: 20,
  },
}));

interface UseCaseOverviewProps {
  selected?: number | undefined;
  setSelected?: (id: number) => void | undefined;
  availableFisaDocuments: AvailableFisaDocumentI[];
  serverCommunicationPending: boolean;
  serverCommunicationError: ErrorMessageI | undefined;
  fisaDocumentsFetched: boolean;
  fetchDocuments: () => void;
  clearCommunicationL: () => void;
  deleteDocumentL: (id: number) => void;
  removable: boolean;
}

function UseCaseOverview({
  selected = undefined,
  setSelected = undefined,
  availableFisaDocuments,
  serverCommunicationPending,
  serverCommunicationError,
  fisaDocumentsFetched,
  fetchDocuments,
  clearCommunicationL,
  deleteDocumentL,
  removable,
}: UseCaseOverviewProps) {
  const classes = styles();

  const [searchText, setSearchText] = React.useState('');
  const [tryToFetch, setTryToFetch] = React.useState(true);
  const [toRemove, setToRemove] = React.useState<AvailableFisaDocumentI | undefined>(undefined);

  const handleListItemClick = (id: number) => {
    setSelected && setSelected(id);
  };

  function searchFilter(availableDocs: AvailableFisaDocumentI[], search: string): AvailableFisaDocumentI[] {
    if (search === '') {
      return availableDocs.sort((a, b) => (a.name > b.name ? 1 : -1));
    }
    return availableDocs
      .filter(doc => doc.name.toLocaleLowerCase().includes(search.toLocaleLowerCase()))
      .sort((a, b) => (a.name > b.name ? 1 : -1));
  }

  const downloadDocument = (id: number) => {
    window.open(`${BackendUrl}/api/documents/${id}/download`, '_blank');
  };

  useEffect(() => {
    if (tryToFetch && !serverCommunicationPending && !fisaDocumentsFetched) {
      fetchDocuments();
      setTryToFetch(false);
      setTimeout(() => setTryToFetch(true), 2000);
    }
  }, [tryToFetch, serverCommunicationPending, fisaDocumentsFetched, fetchDocuments]);

  return (
    <div className={classes.root}>
      <div className={classes.searchBox}>
        <TextField
          value={searchText}
          onChange={e => setSearchText(e.target.value)}
          id="objectSearchBar"
          label={<Translate contentKey="mainPage.search">Search</Translate>}
          variant="outlined"
        />
      </div>
      <Box border={1} className={classes.listBox}>
        <List component="nav" aria-label="main mailbox folders">
          {searchFilter(availableFisaDocuments, searchText).map(fisaDocument => (
            <div key={fisaDocument.id}>
              {!removable && setSelected !== undefined ? (
                <ListItem
                  selected={selected !== undefined && selected === fisaDocument.id}
                  onClick={() => handleListItemClick(fisaDocument.id)}
                  button={true}
                >
                  <ListItemText primary={fisaDocument.name} />
                </ListItem>
              ) : (
                <ListItem
                  selected={selected !== undefined && selected === fisaDocument.id}
                  onClick={() => handleListItemClick(fisaDocument.id)}
                  button={false}
                >
                  <ListItemText primary={fisaDocument.name} />
                  <IconButton
                    aria-label="download"
                    onClick={() => {
                      downloadDocument(fisaDocument.id);
                    }}
                  >
                    <DownloadIcon fontSize="small" />
                  </IconButton>
                  <IconButton
                    disabled={!fisaDocument.owner}
                    aria-label="delete"
                    onClick={() => {
                      setToRemove(fisaDocument);
                    }}
                  >
                    <DeleteIcon fontSize="small" />
                  </IconButton>
                </ListItem>
              )}
            </div>
          ))}
        </List>
      </Box>
      <ErrorSnackbar open={serverCommunicationError !== undefined} error={serverCommunicationError} onClose={() => clearCommunicationL()} />
      <RemoveWarning
        open={toRemove !== undefined}
        nameToRemove={toRemove?.name || ''}
        onNo={() => setToRemove(undefined)}
        onYes={() => {
          if (toRemove) {
            deleteDocumentL(toRemove.id);
          }
          setToRemove(undefined);
        }}
      />
    </div>
  );
}

const mapStateToProps = (state: IRootState) => ({
  fisaDocumentsFetched: getDocumentsFetched(state),
  availableFisaDocuments: getAvailableFisaDocuments(state),
  serverCommunicationPending: getServerCommunicationPending(state),
  serverCommunicationError: getServerCommunicationError(state),
});

const mapDispatchToProps = {
  fetchDocuments: fetchAvailableDocuments,
  clearCommunicationL: clearErrorMessage,
  deleteDocumentL: deleteDocument,
};

export default connect(mapStateToProps, mapDispatchToProps)(UseCaseOverview);
