import React, { useEffect } from 'react';
import { Dialog, DialogContent, DialogTitle, DialogActions, Button } from '@material-ui/core';
import { useDropzone } from 'react-dropzone';
import { connect } from 'react-redux';

import { addDocument, setErrorToShow } from 'app/shared/actions';
import { FisaDocumentI, ErrorMessageI } from 'app/shared/model';
import { Translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import UseCaseOverview from '../useCasesOverview/useCaseOverview';

interface DeveloperMenuProps {
  open: boolean;
  onClose: () => void;
  addNewDocument: (document: FisaDocumentI, userId: number) => void;
  setError: (error: ErrorMessageI) => void;
  userId: number;
}

function DeveloperMenu(props: DeveloperMenuProps) {
  const { getInputProps, open, acceptedFiles } = useDropzone({
    multiple: false,
  });
  const { addNewDocument } = props;

  useEffect(() => {
    if (acceptedFiles.length > 0) {
      acceptedFiles[0]
        .text()
        .then(data => addNewDocument(JSON.parse(data), props.userId))
        .catch(e => props.setError(e));
      acceptedFiles.pop();
    }
  }, [acceptedFiles]);

  return (
    <Dialog open={props.open}>
      <DialogTitle>
        <Translate contentKey="global.menu.devMenuHeading">Developer-Menu</Translate>
      </DialogTitle>
      <DialogContent>
        <UseCaseOverview removable={true} />
      </DialogContent>
      <input {...getInputProps()} />
      <DialogActions>
        <Button onClick={props.onClose}>
          <Translate contentKey="global.menu.close">Close</Translate>
        </Button>
        <Button onClick={() => open()}>
          <Translate contentKey="global.menu.uploadUsecase">Upload Use-Case</Translate>
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const stateToProps = (state: IRootState) => ({
  userId: state.authentication.account.id
});

export default connect(stateToProps, {
  addNewDocument: addDocument,
  setError: setErrorToShow,
})(DeveloperMenu);
