import React, { useEffect } from 'react';
import {
  List,
  ListItem,
  ListItemText,
  IconButton,
  makeStyles,
  Box,
  Dialog,
  DialogTitle,
  DialogActions,
  Button,
  Backdrop,
  CircularProgress,
  DialogContent,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core';
import { connect } from 'react-redux';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';
import DeleteIcon from '@material-ui/icons/Delete';
import DownloadIcon from '@material-ui/icons/GetApp';
import { AvailableProjectI, ErrorMessageI } from 'app/shared/model';
import { loadProjectFromServer, deleteProjectFromBackend, clearErrorMessage, deleteProjectFromBackendAndServer } from 'app/shared/actions';

import {
  getServerCommunicationPending,
  getAvailableProjects,
  getExistsProjectInState,
  getServerCommunicationError,
} from 'app/shared/selectors';

import { IRootState } from 'app/shared/reducers';
import { BackendUrl, Routes } from 'app/shared/util/url-utils';
import { Translate } from 'react-jhipster';
import ErrorSnackbar from '../errorMessages/ErrorSnackbar';
import CreateProjectRoutine from '../createProjectRoutine/CreateProjectRoutine';

const style = makeStyles(theme => ({
  projectsList: {
    width: '60%',
    maxWidth: 500,
    margin: '0 auto',
    padding: 10,
  },
  addButton: {
    marginTop: '20px'
  },
  heading: {
    marginBottom: '2rem',
    marginTop: '1rem',
    fontWeight: 'bolder',
  },
  projectsListItems: {},
  addProjectButton: {
    width: '2rem',
    height: '2rem',
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

interface SavedProjectsOverviewProps {
  availableProjects: AvailableProjectI[];
  projectLoadet: boolean;
  communicationPending: boolean;
  communicationError: ErrorMessageI | undefined;
  deleteProjectFromBackend: (id: number) => Promise<void>;
  deleteProjectFromBackendAndServer: (id: number) => Promise<void>;
  loadProjectFromServer: (id: number) => void;
  clearErrorMessage: () => void;
}

function SavedProjectsOverview(props: SavedProjectsOverviewProps) {
  const history = useHistory();

  const classes = style();
  const [createRoutineOpen, setCreateRoutineOpen] = React.useState(false);
  const [loadObjectActive, setLoadObjectActive] = React.useState(false);
  const [loadError, setLoadError] = React.useState<ErrorMessageI | undefined>(undefined);
  const [removeFromFrostServer, setRemoveFromFrost] = React.useState(false);

  const [projectToRemove, setProjectToRemove] = React.useState<AvailableProjectI | undefined>(undefined);

  const loadProject = (id: number) => {
    props.loadProjectFromServer(id);
    setLoadObjectActive(true);
  };

  const downloadProject = (id: number) => {
    window.open(`${BackendUrl}api/projects/${id}/download`, '_blank');
  };

  useEffect(() => {
    if (loadObjectActive) {
      if (!props.communicationPending && props.projectLoadet) {
        history.push(Routes.PROJECT);
      } else if (!props.communicationPending) {
        setLoadObjectActive(false);
        setLoadError(props.communicationError);
      }
    }
  }, [loadObjectActive, history, props.communicationPending, props.projectLoadet, props.communicationError]);

  return (
    <>
      <Box border={1} className={classes.projectsList}>
        <List component="nav" aria-label="main mailbox folders">
          {props.availableProjects.map(project => (
            <div key={project.id}>
              <ListItem button>
                <ListItemText className={classes.projectsListItems} primary={project.name} onClick={() => loadProject(project.id)} />

                <IconButton
                  aria-label="download"
                  onClick={() => {
                    downloadProject(project.id);
                  }}
                >
                  <DownloadIcon fontSize="small" />
                </IconButton>

                <IconButton onClick={() => setProjectToRemove(project)}>
                  <DeleteIcon />
                </IconButton>
              </ListItem>
            </div>
          ))}
        </List>
      </Box>

      <Button className={classes.addButton} color="secondary" variant="outlined" onClick={() => setCreateRoutineOpen(true)}>
        <AddIcon className={classes.addProjectButton} /><Translate contentKey="home.newProject">New Project</Translate>
      </Button>
      <CreateProjectRoutine open={createRoutineOpen} close={() => setCreateRoutineOpen(false)} />

      <Dialog open={projectToRemove !== undefined} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">
          <Translate contentKey="projectPage.objectRemoveWarning" interpolate={{ name: projectToRemove?.name }}>
            Do you really want to delete &quot;{projectToRemove?.name}&quot;?
          </Translate>
        </DialogTitle>

        <DialogContent>
          <FormControlLabel
            style={{ marginRight: 'auto' }}
            control={<Checkbox checked={removeFromFrostServer} onChange={() => setRemoveFromFrost(oldData => !oldData)} />}
            label={<Translate contentKey="projectPage.removeFromFrost">Delete also from FROST-Server</Translate>}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={() => setProjectToRemove(undefined)} color="primary">
            <Translate contentKey="global.menu.no">No</Translate>
          </Button>
          <Button
            onClick={() => {
              if (projectToRemove) {
                if (removeFromFrostServer) {
                  props.deleteProjectFromBackendAndServer(projectToRemove.id);
                } else {
                  props.deleteProjectFromBackend(projectToRemove.id);
                }
              }
              setProjectToRemove(undefined);
            }}
            color="primary"
            autoFocus
          >
            <Translate contentKey="global.menu.yes">Yes</Translate>
          </Button>
        </DialogActions>
      </Dialog>

      <ErrorSnackbar
        open={loadError !== undefined}
        error={loadError}
        onClose={() => {
          setLoadError(undefined);
          props.clearErrorMessage();
        }}
      />

      <Backdrop className={classes.backdrop} open={loadObjectActive}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
}

const mapDispatchToProps = {
  deleteProjectFromBackend,
  loadProjectFromServer,
  clearErrorMessage,
  deleteProjectFromBackendAndServer,
};

const mapStateToProps = (state: IRootState) => ({
  availableProjects: getAvailableProjects(state),
  projectLoadet: getExistsProjectInState(state),
  communicationPending: getServerCommunicationPending(state),
  communicationError: getServerCommunicationError(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(SavedProjectsOverview);
