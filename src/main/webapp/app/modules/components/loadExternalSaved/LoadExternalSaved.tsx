import React, { useEffect } from 'react';
import { Button, ButtonGroup } from '@material-ui/core';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useDropzone } from 'react-dropzone';

import { FISA_OBJECTS } from 'app/variables/variables';
import { loadAutoSave, loadFromPC } from 'app/shared/actions';
import { FisaProjectI, ErrorMessageI } from 'app/shared/model';
import { Routes } from 'app/shared/util/url-utils';
import { Translate } from 'react-jhipster';
import ErrorSnackbar from '../errorMessages/ErrorSnackbar';


interface LoadExternalSavedProps {
  loadFromAutoSave: () => void;
  loadProjectFromPC: (project: FisaProjectI) => void;
}

function LoadExternalSaved(props: LoadExternalSavedProps) {
  const [errorSnackbarOpen, setErrorSnackbarOpen] = React.useState(false);
  const [error, setError] = React.useState<ErrorMessageI | undefined>(undefined);
  const { getInputProps, open, acceptedFiles } = useDropzone({
    multiple: false,
  });
  const history = useHistory();
  const { loadProjectFromPC } = props;

  const loadFromAutoSave = () => {
    props.loadFromAutoSave();
    setTimeout(() => {
      history.push(Routes.PROJECT);
    }, 100);
  };

  useEffect(() => {
    if (acceptedFiles.length > 0) {
      acceptedFiles[0]
        .text()
        .then(data => loadProjectFromPC(JSON.parse(data)))
        .then(() => history.push(Routes.PROJECT))
        .catch(e => {
          setError(e);
          setErrorSnackbarOpen(true);
        });
      acceptedFiles.pop();
    }
  }, [acceptedFiles, loadProjectFromPC, history]);

  const canLoadFromLocalStore = () => {
    const itemsFromStore = localStorage.getItem(FISA_OBJECTS);
    return itemsFromStore !== undefined && itemsFromStore !== null && itemsFromStore.length > 0;
  };
  return (
    <>
      <ButtonGroup orientation="vertical" color="primary" aria-label="vertical outlined primary button group">
        <Button disabled={!canLoadFromLocalStore()} onClick={() => loadFromAutoSave()}>
          <Translate contentKey="global.menu.loadLastProject">Load Recent Project</Translate>
        </Button>

        <Button onClick={open}>
          <Translate contentKey="global.menu.loadFromPC">Load from PC</Translate>
        </Button>
      </ButtonGroup>

      <input {...getInputProps()} />
      <ErrorSnackbar open={errorSnackbarOpen} onClose={() => setErrorSnackbarOpen(false)} error={error} />
    </>
  );
}

const dispatchToProps = {
  loadFromAutoSave: loadAutoSave,
  loadProjectFromPC: loadFromPC,
};

export default connect(null, dispatchToProps)(LoadExternalSaved);
