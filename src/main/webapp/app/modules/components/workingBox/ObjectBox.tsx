import React from 'react';
import { connect } from 'react-redux';
import {
  Card,
  CardActions,
  CardContent,
  Button,
  IconButton,
  List,
  ListItem,
  CardHeader,
  makeStyles,
  Tooltip,
  Typography,
} from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';

import clsx from 'clsx';
import { getDontAskOnObjectDelete, getHighlightedObject} from 'app/shared/selectors';
import { getBackgroundColor, getHighlightBackgroundColor } from 'app/variables/colors';
import { setHighlightedObject, enableScrollingAction, setObjectActive } from 'app/shared/actions';
import { Translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import AttributeBox from './AttributeBox';
import RemoveWarning from './ObjectRemoveWarning';
import { ObjectBoxProps, ObjectBoxBaseProps } from './workingBoxInterfaces';
import FrostLink from './FrostLink';

/**
 * The Boxes of Objects inside the WorkingBox
 */

function ObjectBox(props: ObjectBoxProps) {
  const classes = makeStyles(theme => ({
    card: {
      backgroundColor: props.object.id === props.highlightedObject ? getHighlightBackgroundColor(theme) : getBackgroundColor(theme),
    },
    bordered: {
      borderStyle: 'solid',
      borderColor: theme.palette.primary.dark,
    },
  }))();

  const [warningOpen, setWarningOpen] = React.useState(false);
  const [mapDialogOpen, setMapDialogOpen] = React.useState(false);

  const deleteObject = (removeFromFrost: boolean) => {
    setWarningOpen(false);
    props.removeObject(props.object.id, removeFromFrost);
  };

  const closeWarning = () => setWarningOpen(false);
  const openWarning = () => (props.dontShowWarning && !props.object.frostId ? deleteObject(false) : setWarningOpen(true));

  const checkIfGoToObject = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (props.object.isLinked) {
      event.preventDefault();
      props.dispatch(enableScrollingAction());
      props.dispatch(setObjectActive(props.object.parent));
      setTimeout(() => props.dispatch(setHighlightedObject(props.object.id)), 100);
    }
  };

  const objectBoxClassName = props.object.frostId ? clsx(classes.card, classes.bordered) : classes.card;

  const objectBox = (
    <Card
      className={objectBoxClassName}
      onMouseEnter={() => props.dispatch(setHighlightedObject(props.object.id))}
      onClick={checkIfGoToObject}
    >
      <CardHeader
        title={
          <Typography variant="h6" display="block" color="textPrimary" style={{ maxWidth: 'calc(100% - 5px' }}>
            {props.object.nameToShow}
          </Typography>
        }
        action={
          <IconButton
            size="small"
            onClick={e => {
              e.stopPropagation();
              openWarning();
            }}
          >
            <DeleteOutlineIcon />
          </IconButton>
        }
      />

      <CardContent>
        <List>
          {props.object.attributes.map(attribute => (
            <ListItem key={attribute.definitionName}>
              <AttributeBox
                disabled={props.object.isLinked}
                objectId={props.object.id}
                attributeDefinitionName={attribute.definitionName}
                changeObjectProperty={(key: string, value: string) => props.changeObjectProperty(props.object.id, key, value)}
              />
            </ListItem>
          ))}
          {props.object.frostId && <FrostLink ogcType={props.ogcType} frostId={props.object.frostId} />}
        </List>
        <CardActions>
          {props.object.selectable && (
            <Button variant="outlined" disabled={props.object.isLinked} size="small" onClick={() => props.goToObject(props.object.id)}>
              <Translate contentKey="projectPage.goToObject">Go to object</Translate>
            </Button>
          )}
        </CardActions>
      </CardContent>
    </Card>
  );

  return (
    <>
      {props.object.isLinked ? (
        <Tooltip title={<Translate contentKey="projectPage.objectLinked">This Object is Linked</Translate>}>{objectBox}</Tooltip>
      ) : (
        objectBox
      )}
      <RemoveWarning warningOpen={warningOpen} closeWarning={closeWarning} deleteObject={deleteObject} object={props.object} />
    </>
  );
}

const propsToState = (state: IRootState, baseProps: ObjectBoxBaseProps) => ({
  dontShowWarning: getDontAskOnObjectDelete(state),
  highlightedObject: getHighlightedObject(state),
  ...baseProps,
});

export default connect(propsToState)(ObjectBox);
