/* eslint-disable react/jsx-no-target-blank */
import { Link, makeStyles } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { getConnectedFrostServer } from 'app/shared/selectors';
import { IRootState } from 'app/shared/reducers';
import { Translate } from 'react-jhipster';

const styles = makeStyles(() => ({
  link: {
    textDecoration: 'none',
  },
}));

interface FrostLinkProps {
  ogcType: string;
  frostUrl: string | undefined;
  frostId: number | undefined;
}

const FrostLink = (props: FrostLinkProps) => {
  const classes = styles();
  let type: string;

  switch (props.ogcType) {
    case 'FeaturesOfInterest':
      type = 'FeaturesOfInterest';
      break;
    case 'ObservedProperty':
      type = 'ObservedProperties';
      break;
    default:
      if (props.ogcType.endsWith('s')) {
        type = props.ogcType;
      } else {
        type = `${props.ogcType}s`;
      }
  }
  const address = `${props.frostUrl}/${type}(${props.frostId})`;

  return (
    <a className={classes.link} href={address} target="_blank" rel="noopener">
      <Link variant="body1">
        <Translate contentKey="projectPage.goToFrostLocation">Go to FROST-Server</Translate>
      </Link>
    </a>
  );
};

const stateToProps = (state: IRootState) => ({
  frostUrl: getConnectedFrostServer(state),
});

export default connect(stateToProps)(FrostLink);
