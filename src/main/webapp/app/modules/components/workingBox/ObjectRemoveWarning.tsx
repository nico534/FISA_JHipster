import React from 'react';
import { Checkbox, DialogContent, FormControlLabel, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import { dontShowObjectRemoveWarning } from 'app/shared/actions';
import { ObjectWithNameI } from 'app/shared/model';
import { Translate, translate } from 'react-jhipster';
import RemoveWarning from '../errorMessages/RemoveWarning';

interface RemoveWarningProps {
  warningOpen: boolean;
  closeWarning: () => void;
  deleteObject: (removeFromFrost: boolean) => void;
  dontShowObjectRemoveWarning: () => void;
  object: ObjectWithNameI;
}

function ObjectRemoveWarning(props: RemoveWarningProps) {
  const [dontWarn, setDontWarn] = React.useState(false);
  const [removeFromFrost, setRemoveFromFrost] = React.useState(true);

  const warningIsOpen = props.warningOpen;
  const dontShowRemoveWarning = props.dontShowObjectRemoveWarning;
  React.useEffect(() => {
    if (!warningIsOpen && dontWarn) {
      dontShowRemoveWarning();
    }
  }, [dontWarn, warningIsOpen, dontShowRemoveWarning]);

  return (
    <RemoveWarning
      open={props.warningOpen}
      onNo={() => props.closeWarning()}
      onYes={() => props.deleteObject(removeFromFrost)}
      nameToRemove={props.object.nameToShow}
    >
      {!props.object.frostId && (
        <FormControlLabel
          style={{ marginRight: 'auto' }}
          control={<Checkbox checked={dontWarn} onChange={() => setDontWarn(!dontWarn)} />}
          label={translate("global.menu.dontWarnAgain")}
        />
      )}
      {props.object.frostId && (
        <>
          <Typography>
            <Translate contentKey="projectPage.removeFrostLinkedObjectMessage">
              This Object is linked to an Entity on a FROST-Server!
            </Translate>
          </Typography>
          <DialogContent>
            <FormControlLabel
              style={{ marginRight: 'auto' }}
              control={<Checkbox checked={removeFromFrost} onChange={() => setRemoveFromFrost(oldData => !oldData)} />}
              label={<Translate contentKey="projectPage.removeFromFrost">Delete also from FROST-Server</Translate>}
            />
          </DialogContent>
        </>
      )}
    </RemoveWarning>
  );
}

export default connect(null, { dontShowObjectRemoveWarning })(ObjectRemoveWarning);
