import React from 'react';
import { connect } from 'react-redux';
import {
  TextField,
  TableCell,
  List, ListItem, Button, makeStyles,
} from '@material-ui/core';

import { AttributeI, PolygonI, PointI } from 'app/shared/model';
import { changeObjectProperty } from 'app/shared/actions';
import { Translate } from 'react-jhipster';
import MapDialog from '../map/MapDialog';
import {control} from "leaflet";

const style = makeStyles(() => ({
  noPaddingMargin: {
    padding: 0,
    margin: 0,
  },
  accordion: {
    padding: 0,
    margin: 0,
    backgroundColor: 'inherit',
    width: '100%',
  },
  addNewIcon: {
    transform: 'scale(1.5)',
  },
  mapButton: {
    padding: 0,
    textAlign: 'center',
  },
}));

interface PointPositionAttributeProps {
  attribute: AttributeI,
  disabled: boolean,
  objectId: number,
  changeObjectProperty: (objectId: number, key: string, value: PointI) => void;
}

const PointPositionAttribute = (props: PointPositionAttributeProps) => {
  const classes = style();
  const [mapOpen, setMapOpen] = React.useState(false);

  return(
    <>
      <List>
        <ListItem className={classes.noPaddingMargin}>
          <PositionField changeAttribute={
            value => props.changeObjectProperty(
              props.objectId,
              props.attribute.definitionName,
              value)
          } pos={props.attribute.value as PointI} disabled={props.disabled} />
        </ListItem>
        <ListItem className={classes.noPaddingMargin}>
          <Button className={classes.mapButton} disabled={props.disabled} size="small" variant="outlined" onClick={() => setMapOpen(true)}>
            <Translate contentKey="projectPage.getPositionsFromCard">get Positions from Card</Translate>
          </Button>
        </ListItem>
      </List>

      <MapDialog
        positionData={props.attribute.value as PointI}
        isPolygone={false}
        objectId={props.objectId}
        open={mapOpen}
        close={() => setMapOpen(false)}
        assignPosition={value => props.changeObjectProperty(
          props.objectId,
          props.attribute.definitionName,
          value as PointI
        )}
      />
    </>
  )
}

export interface PositionFieldProps {
  changeAttribute: (value: PointI) => void;
  pos: PointI;
  disabled: boolean;
}

export function PositionField(props: PositionFieldProps) {
  const changeObjectEvent = (value: string, index: number, isBadInput: boolean) => {
    if (!isBadInput) {
      const newval = props.pos;
      newval[index] = (value as unknown) as number;

      props.changeAttribute(newval);
    }
  };
  return (
    <>
      <TableCell padding="none" align="right">
        <TextField
          disabled={props.disabled}
          id="standard-basic"
          type="number"
          value={props.pos[0]}
          onChange={e => changeObjectEvent(e.target.value, 0, e.target.validity.badInput)}
        />
      </TableCell>
      <TableCell padding="none" align="right">
        <TextField
          disabled={props.disabled}
          id="standard-basic"
          type="number"
          value={props.pos[1]}
          onChange={e => changeObjectEvent(e.target.value, 1, e.target.validity.badInput)}
        />
      </TableCell>
    </>
  );
}

const actionsToProps = {
  changeObjectProperty,
}

export default connect(null, actionsToProps)(PointPositionAttribute);
