import React, { Dispatch, RefObject } from 'react';

import { ObjectWithNameI, ObjectsCategoryI, ActionI, PointI } from 'app/shared/model';

export interface ObjectBoxBaseProps {
  object: ObjectWithNameI;
  removeObject: (objectId: number, removeFromFrost: boolean) => void;
  changeObjectProperty: (objectId: number, key: string, value: string) => void;
  goToObject: (objectId: number) => void;
}

export interface ObjectBoxProps extends ObjectBoxBaseProps {
  dispatch: Dispatch<ActionI>;
  dontShowWarning: boolean;
  highlightedObject: number;
  ogcType: string;
}

export interface ObjectBoxCategoryProps {
  dispatch: Dispatch<ActionI>;
  objectCategory: ObjectsCategoryI;
  scroll: (options?: ScrollOptions) => void;
}

export interface WorkingBoxProps {
  scroll: (options?: ScrollOptions) => void;
  dispatch: Dispatch<ActionI>;
  objectCategories: ObjectsCategoryI[];
  highlightedObject: number;
}

interface ScrollOptions {
  id?: string;
  ref?: React.RefObject<unknown>;
  x?: number;
  y?: number;
  smooth?: boolean;
}

export interface ObjectGridProps {
  dispatch: Dispatch<ActionI>;
  objects: ObjectWithNameI[];
  ogcType: string;
  definitionName: string;
  withAddIcon: boolean;
  scroll: (options?: ScrollOptions) => void;
  parentRef: RefObject<HTMLDivElement>;
}

export interface ObjectBoxFrameProps {
  scroll: (options?: ScrollOptions) => void;
  dispatch: Dispatch<ActionI>;
  ogcType: string;
  object: ObjectWithNameI;
  highlightedObject: number;
  parentRef: RefObject<HTMLDivElement>;
  scrollingActive: boolean;
}
