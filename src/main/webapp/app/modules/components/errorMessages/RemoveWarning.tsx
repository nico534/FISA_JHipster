import React from 'react';
import { DialogTitle, Dialog, DialogActions, Button, DialogContent } from '@material-ui/core';
import { Translate } from 'react-jhipster';

export default function RemoveWarning({
  open,
  nameToRemove,
  onNo,
  onYes,
  children = <div />,
}: {
  open: boolean;
  nameToRemove: string;
  onNo: () => void;
  onYes: () => void;
  // eslint-disable-next-line react/require-default-props
  children?: React.ReactNode;
}) {
  // const { t } = useTranslation('general');
  return (
    <Dialog open={open} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
      <DialogTitle id="alert-dialog-title">
        <Translate contentKey="warning.removeWarning" interpolate={{ name: nameToRemove }}>
          Do you really wont to remove &quot;{nameToRemove}&quot;?
        </Translate>
      </DialogTitle>

      <DialogContent>{children}</DialogContent>

      <DialogActions>
        <Button onClick={onNo} color="primary" autoFocus>
          <Translate contentKey="global.menu.no">No</Translate>
        </Button>
        <Button onClick={onYes} color="primary">
          <Translate contentKey="global.menu.yes">Yes</Translate>
        </Button>
      </DialogActions>
    </Dialog>
  );
}
