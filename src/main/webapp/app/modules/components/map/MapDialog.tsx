import { Dialog, DialogContent, DialogActions, Button, DialogTitle, makeStyles, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import React, { Dispatch } from 'react';

import { CardPositionI, ActionI, PolygonI, PointI } from 'app/shared/model';
import { changeObjectProperty } from 'app/shared/actions';
import { getAllCardPositions } from 'app/shared/selectors';
import { Translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import MapComponent from './OverviewMap';

const style = makeStyles(theme => ({
  backgroundComponent: {
    backgroundColor: theme.palette.background.default,
  },
}));

interface MapDialogOuterProps {
  open: boolean;
  close: () => void;
  objectId: number;
  isPolygone: boolean;
  assignPosition: (value: PointI | PolygonI) => void;
}

interface MapDialogProps extends MapDialogOuterProps {
  dispatch: Dispatch<ActionI>;
  positionData: PointI | PolygonI;
  allOtherPositions: CardPositionI[];
}

function MapDialog(props: MapDialogProps) {
  const classes = style();
  const [position, setPosition] = React.useState(props.positionData);

  const assignPosition = () => {
    props.close();
    props.assignPosition(position);
    /*
    if (!props.isPolygone) {
      const pos = position as PointI;

      props.dispatch()
      props.dispatch(changeObjectProperty(props.objectId, props.positionKeys[0], pos[0]));
      props.dispatch(changeObjectProperty(props.objectId, props.positionKeys[1], pos[1]));
    } else if (props.isPolygone && props.polyPositionKey) {
      const pos = position as PolygonI;
      props.dispatch(changeObjectProperty(props.objectId, props.polyPositionKey, pos));
    }
     */
  };

  return (
    <Dialog fullWidth={true} maxWidth={false} open={props.open} onEntering={() => setPosition(props.positionData)}>
      <DialogTitle className={classes.backgroundComponent}>
        {props.isPolygone ? (
          <Translate contentKey="maps.polyCta">
            Click to add a new location to the polygon. You can drag and delete existing ones.
          </Translate>
        ) : (
          <Translate contentKey="maps.cta">Pick a location and submit</Translate>
        )}
      </DialogTitle>
      <DialogContent className={classes.backgroundComponent} style={{ height: '80vh' }}>
        <MapComponent
          latLng={!props.isPolygone ? (position as PointI) : undefined}
          polyLatLng={props.isPolygone ? (position as PolygonI) : undefined}
          setLatLng={(latLng: PointI) => setPosition(latLng)}
          setPolyLatLng={(polyLatLng: PolygonI) => setPosition(polyLatLng)}
          otherPositions={props.allOtherPositions}
        />
      </DialogContent>
      <DialogActions className={classes.backgroundComponent}>
        {!props.isPolygone && (
          <Typography style={{ marginRight: 'auto' }}>
            <Translate contentKey="maps.currentLocation">Current Location: </Translate>
            {`[${position[0]} lat, ${position[1]} lng]`}
          </Typography>
        )}
        {props.isPolygone && (
          <Button variant="outlined" onClick={() => setPosition([])} style={{ marginRight: 'auto' }}>
            <Translate contentKey="maps.clearPolygon">Clear polygon</Translate>
          </Button>
        )}
        <Button variant="outlined" color="primary" onClick={() => props.close()}>
          <Translate contentKey="global.menu.cancel">Cancel</Translate>
        </Button>
        <Button variant="outlined" color="primary" onClick={() => assignPosition()} autoFocus>
          <Translate contentKey="global.menu.confirm">Ok</Translate>
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const stateToProps = (state: IRootState, props: MapDialogOuterProps) => ({
  allOtherPositions: getAllCardPositions(state).filter(pos => pos.objectId !== props.objectId),
});

export default connect(stateToProps)(MapDialog);
