// eslint-disable-next-line no-use-before-define
import React from 'react';
import { connect } from 'react-redux';
import { Prompt, useHistory } from 'react-router-dom';
import { getExistsProjectInState, getNotSaved } from 'app/shared/selectors';
import { Routes } from 'app/shared/util/url-utils';
import ProjectPage from './ProjectPage';
import {translate} from "react-jhipster";

interface ProjectPageLoaderProps {
  projectExists: boolean;
  notSaved: boolean;
}

/**
 * The ProjectPage where the project gets defined
 */
class ProjectPageLoader extends React.Component<ProjectPageLoaderProps> {

  /**
   * ActionI to stop leaving page and ask if the user wont to save first.
   * @param {*} e
   */
  onUnload = (e) => {
    const confirmMsg = 'Do you really want to leaf FISA?';
    e.preventDefault();
    e.returnValue = confirmMsg;
    return confirmMsg;
  };

  /**
   * Add "ask if the user wonts to leaf page" after Page did mount
   */
  componentDidMount() {
    // eslint-disable-next-line no-console
    console.log('component mount');

    window.addEventListener('beforeunload', this.onUnload);
  }

  /**
   * Remove "ask if the user wonts to leaf page" after Page did unmount
   */

  componentWillUnmount() {
    // eslint-disable-next-line no-console
    console.log('component will unmount');
    window.removeEventListener('beforeunload', this.onUnload);
  }


  render() {
    return this.props.projectExists ? (
      <div>
        <Prompt
          when={this.props.notSaved}
          message={() => translate("global.sureYouWantLeaf")}
        />
        <ProjectPage />
      </div>
    ) : (
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      <ToHomePage />
    );
  }
}

function ToHomePage() {
  const history = useHistory();
  history.push(Routes.ROOT);
  return null;
}

const mapStateToProps = (state) => ({
  projectExists: getExistsProjectInState(state),
  notSaved: getNotSaved(state),
});

export default connect(mapStateToProps)(ProjectPageLoader);
