import './home.scss';

import React from 'react';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Row, Col } from 'reactstrap';

export const StartPage = () => {
  return (
    <Row>
      <Col md="9">
        <h1>
          <Translate contentKey="home.title">Welcome, Java Hipster!</Translate>
        </h1>
        <p className="lead">
          <Translate contentKey="home.subtitle">FISA is a tool to setup a SensorThings-Datamodel on a visual basis.</Translate>
        </p>

        <p/>

        <p>
          <Translate contentKey="home.history">
            FISA was developed as a PSE-Project at the FraunhoferIOSB.
          </Translate>
        </p>

        <p>
          <Translate contentKey="home.ourFeatures">Features</Translate>
        </p>

        <ul>
          <li>
            <Translate contentKey="home.features.defineUsecases">Define Usecases with the help of a FISA-Document</Translate>
          </li>
          <li>
            <Translate contentKey="home.features.cloneEntities">Clone already defined Entities</Translate>
          </li>
          <li>
            <Translate contentKey="home.features.linkEntities">Link already defined Entities</Translate>
          </li>
          <li>
            <Translate contentKey="home.features.pickLocation">Pick a Location-Point or Polygon with the help of a Card-Function</Translate>
          </li>
          <li>
            <Translate contentKey="home.features.uploadProject">Upload the finished Project to your FROST-Server (create the Entities)</Translate>
          </li>
          <li>
            <Translate contentKey="home.features.saveProject">Save the Project to change it later</Translate>
          </li>
          <li>
            <Translate contentKey="home.features.changeProject">Go back later and change your project. The changes will be added to your FROST-Server.</Translate>
          </li>
        </ul>

        <p>
          <Translate contentKey="home.like">If you like FISA visit the Project on GitHub</Translate>{' '}
          <a href="https://gitlab.com/nico534/FISA_JHipster" target="_blank" rel="noopener noreferrer">
            GitLab
          </a>
          !
        </p>
        <p><Translate contentKey="home.ogcServer.heading">FISA works on all Servers witch use the OGC SensorThings API</Translate></p>

        <p><Translate contentKey="home.ogcServer.tested">It is testet with the FROST-Server of the</Translate>{' '} <a href="https://github.com/FraunhoferIOSB/FROST-Server">GitHub</a></p>

        <p>
          <Translate contentKey="home.ogcServer.entities">
            FISA can handle the following OGC-Entity types:
          </Translate>
        </p>
        <ul>
          <li>Thing</li>
          <li>Sensor</li>
          <li>Datastream</li>
          <li>Location</li>
          <li>HistoricalLocation</li>
          <li>FeatureOfInterest</li>
          <li>ObservedProperty</li>
          <li>Observation</li>
        </ul>
      </Col>
    </Row>
  );
};

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
});

export default connect(mapStateToProps)(StartPage);
