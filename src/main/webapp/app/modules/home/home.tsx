import './home.scss';

import React from 'react';
import { connect } from 'react-redux';
import { StartPage } from 'app/modules/home/startPage';
import MainPage from 'app/modules/mainPage/MainPage';
import Header from 'app/shared/layout/header/header';
import {Translate} from "react-jhipster";

export type IHomeProp = StateProps;

export const Home = (props: IHomeProp) => {
  const { account } = props;

  let page = <StartPage />;

  if (account && account.login) {
    if(props.isAuthenticated) {
      page = <MainPage />;
    } else {
      page = (
        <div className="insufficient-authority">
          <div className="alert alert-danger">
            <Translate contentKey="error.http.403">You are not authorized to access this page.</Translate>
          </div>
        </div>
      );
    }
  }

  return page;
};

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Home);
