import { IRootState } from 'app/shared/reducers';

export const getServerCommunicationActive = (state: IRootState) => state.serverCommunication.active;

export const getServerCommunicationPending = (state: IRootState) => state.serverCommunication.pending;

export const getServerCommunicationError = (state: IRootState) => state.serverCommunication.error;

export const getProjectsFetched = (state: IRootState) => state.availableFisaDocumentsProjects.projectsFetched;

export const getDocumentsFetched = (state: IRootState) => state.availableFisaDocumentsProjects.documentsFetched;
