import { Theme } from '@material-ui/core';
import { IRootState } from 'app/shared/reducers';

/**
 *
 * @param state The redux IRootState of the program
 * @returns true if dontAskAnObjectDelete is set
 */
export const getDontAskOnObjectDelete = (state: IRootState): boolean => state.projectPage.dontShowObjectRemoveWarning;

/**
 *
 * @param state The redux IRootState of the program
 * @returns the current theme
 */
// export const getTheme = (state: IRootState): Theme => state.projectPage.theme;

/**
 *
 * @param state The redux IRootState of the program
 * @returns the name of the current theme
 */
// export const getThemeName = (state: IRootState): string => state.projectPage.themeName;

/**
 *
 * @param state The redux IRootState of the program
 * @returns true if the undo history is not empty
 */
export const getCanUndo = (state: IRootState): boolean => state.fisaProject.undoHistory.length > 0;

/**
 *
 * @param state The redux IRootState of the program
 * @returns true if the redo history is not empty
 */
export const getCanRedo = (state: IRootState): boolean => state.fisaProject.redoHistory.length > 0;

/**
 *
 * @param state The redux IRootState of the program
 * @returns true if there are unsaved changes
 */
export const getNotSaved = (state: IRootState): boolean => state.projectPage.notSaved;

/**
 *
 * @param state The redux IRootState of the program
 * @returns the object-id of the currently highlighted object
 */
export const getHighlightedObject = (state: IRootState): number => state.projectPage.highlightedObject;

export const getScrollingActionActive = (state: IRootState): boolean => state.projectPage.scrollingActive;

export const getProjectExistsOnBackend = (state: IRootState) => state.projectPage.projectExistsOnBackend;

export const getDatastremConnectionData = (state: IRootState) => state.availableFisaDocumentsProjects.datastreamConnectData;
