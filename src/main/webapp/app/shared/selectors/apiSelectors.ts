import { ERROR_MAPS_TO } from 'app/variables/variables';
import { POLY_POSITION, POINT_POSITION } from 'app/variables/valueTypes';
import { IRootState } from 'app/shared/reducers';
import {
  AttributeI,
  BackendAttributeI,
  BackendFisaObjectI,
  FisaObjectI,
  FisaProjectI,
  FisaDocumentI,
  FisaObjectDefinitionI,
  AttributesDefinitionI,
} from '../model';

/**
 * Maps a frontend-attribute with additional properties to a simpler backend-attribute
 *
 * @param attribute
 */
function attributeToBackendAttribute(attribute: AttributeI): BackendAttributeI {
  if (attribute.valueType === POLY_POSITION || attribute.valueType === POINT_POSITION) {
    return {
      definitionName: attribute.definitionName,
      value: JSON.stringify(attribute.value),
    };
  }
  return {
    definitionName: attribute.definitionName,
    value: attribute.value,
  };
}

/**
 * Maps a frontend-attribute with additional properties to a simpler backend-attribute
 *
 * @param attribute
 */
function predefinedAttributeDefToBackendAttribute(attribute: AttributesDefinitionI): BackendAttributeI {
  return {
    definitionName: attribute.name,
    value: attribute.value || '',
  };
}

/**
 * Maps a frontend-object to backend-object
 *
 * @param object the Fisa-Object interface from the Fronend
 * @param predefinedAttributes the List of already predefined
 * @param ignoreFrostIds
 */
function objectToBackendObject(
  object: FisaObjectI,
  predefinedAttributes: AttributesDefinitionI[],
  ignoreFrostIds: boolean
): BackendFisaObjectI {
  return {
    frostId: ignoreFrostIds ? undefined : object.frostId,
    id: object.id,
    definitionName: object.definitionName,
    attributes: [
      ...object.attributes.map(attribute => attributeToBackendAttribute(attribute)),
      ...predefinedAttributes.map(attribute => predefinedAttributeDefToBackendAttribute(attribute)),
    ],
    children: object.children.map(child => child.id),
  };
}

function getPredefinedAttributes(definitions: FisaObjectDefinitionI[], definitioName: string): AttributesDefinitionI[] {
  const predefinedAttributes = definitions.find(def => def.name === definitioName)?.attributes.filter(attribute => attribute.isPredefined);
  if (!predefinedAttributes) {
    return [];
  }
  return predefinedAttributes;
}

/**
 * Gets a FisaProjectI in Backend-Format to be uploaded to the Backend
 *
 * @param state the Root-State of the Project
 * @param withExampleData - if example-data should be generated
 * @param ignoreFrostIds - if Frost-Ids should be ignored
 */
export const getFisaProjectFromState = (state: IRootState, withExampleData: boolean, ignoreFrostIds: boolean): FisaProjectI => {
  const fisaObjects: BackendFisaObjectI[] = state.fisaProject.objects.active
    .filter(object => object.id !== 0)
    .map(object =>
      objectToBackendObject(
        object,
        getPredefinedAttributes(state.fisaProject.constantParts.objectDefinitions, object.definitionName),
        ignoreFrostIds
      )
    );

  const removedFisaObjects: BackendFisaObjectI[] = state.fisaProject.objects.removed.map(object =>
    objectToBackendObject(
      object,
      getPredefinedAttributes(state.fisaProject.constantParts.objectDefinitions, object.definitionName),
      ignoreFrostIds
    )
  );

  const fisaObjectDefinition = state.fisaProject.constantParts.objectDefinitions
    .filter(definition => definition.mapsTo !== ERROR_MAPS_TO)
    .map(definition => ({
      ...definition,
      mapsTo: definition.mapsTo,
      attributes: definition.attributes,
    }));

  const fisaDocument: FisaDocumentI = {
    id: state.fisaProject.constantParts.fisaDocumentId,
    name: state.fisaProject.constantParts.fisaDocumentName,
    objectDefinitions: fisaObjectDefinition,
    fisaTemplate: [],
  };
  return {
    id: state.fisaProject.id,
    connectedFrostServer: state.fisaProject.connectedFrostServer,
    fisaDocument,
    name: state.fisaProject.constantParts.fisaProjectName,
    fisaObjects,
    removedFisaObjects: ignoreFrostIds ? [] : removedFisaObjects,
    generateExampleData: withExampleData,
  };
};
