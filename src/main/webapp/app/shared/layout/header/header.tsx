import './header.scss';

import React, { useState } from 'react';
import { Translate, Storage } from 'react-jhipster';
import { Navbar, Nav, NavbarToggler, Collapse } from 'reactstrap';
import { connect } from 'react-redux';

import LoadingBar from 'react-redux-loading-bar';
import {useLocation} from "react-router-dom";

import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { setLocale } from 'app/shared/reducers/locale';
import { AdminMenu, AccountMenu, LocaleMenu } from '../menus';
import { Brand } from './header-components';
import {PROJECT} from "app/variables/routes";
import {IconButton, makeStyles, Tooltip} from "@material-ui/core";
import UndoIcon from "@material-ui/icons/Undo";
import RedoIcon from "@material-ui/icons/Redo";
import SaveMenu from "app/shared/layout/menus/SaveMenu";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import UploadToFrostDialog from "app/modules/components/uploadToFrostRoutine/UploadToFrostDialog";
import {getCanRedo, getCanUndo} from "app/shared/selectors";
import {redo, undo} from "app/shared/actions";

export interface IHeaderProps {
  isAuthenticated: boolean;
  isAdmin: boolean;
  ribbonEnv: string;
  isInProduction: boolean;
  isSwaggerEnabled: boolean;
  currentLocale: string;
  onLocaleChange: Function;
  canUndo: boolean;
  canRedo: boolean;
  undo: () => void;
  redo: () => void;
}

const style = makeStyles(() => ({
  space: {
    marginRight: '40px',
    height: '100%',
    padding: '7px 10px'
  },
}));

const Header = (props: IHeaderProps) => {
  const classes = style();
  const [menuOpen, setMenuOpen] = useState(false);

  const [uploadDialogOpen, setUploadDialogOpen] = React.useState(false);

  const location = useLocation();

  const uploadToFisa = () => {
    setUploadDialogOpen(true);
  };

  const handleLocaleChange = event => {
    const langKey = event.target.value;
    Storage.session.set('locale', langKey);
    props.onLocaleChange(langKey);
  };

  const renderDevRibbon = () =>
    props.isInProduction === false ? (
      <div className="ribbon dev">
        <a href="">
          <Translate contentKey={`global.ribbon.${props.ribbonEnv}`} />
        </a>
      </div>
    ) : null;

  const toggleMenu = () => setMenuOpen(!menuOpen);

  /* jhipster-needle-add-element-to-menu - JHipster will add new menu items here */

  return (
    <div id="app-header">
      {renderDevRibbon()}
      <LoadingBar className="loading-bar" />
      <Navbar style={{ padding: '0.5rem 1rem' }} dark expand="sm" fixed="top" className="bg-dark">
        <NavbarToggler aria-label="Menu" onClick={toggleMenu} />
        <Brand />
        <Collapse isOpen={menuOpen} navbar>
          <Nav id="header-tabs" className="ml-auto" navbar>
            {props.isAuthenticated && location.pathname.startsWith(PROJECT) && (
              <div style={{ marginLeft: 'auto' }}>
                <Tooltip title={<Translate contentKey="global.menu.undo">Undo</Translate>}>
                  <span>
                    <IconButton disabled={!props.canUndo} style={{ color: props.canUndo ? '#fff': '#808080' }} onClick={() => props.undo()} color="inherit">
                      <UndoIcon />
                    </IconButton>
                  </span>
                </Tooltip>

                <Tooltip title={<Translate contentKey="global.menu.redo">Redo</Translate>}>
                  <span className={classes.space}>
                    <IconButton disabled={!props.canRedo} style={{ color: props.canRedo ? '#fff': '#808080' }} onClick={() => props.redo()} color="inherit">
                      <RedoIcon />
                    </IconButton>
                  </span>
                </Tooltip>

                <SaveMenu />

                <Tooltip title={<Translate contentKey="general.menu.toFrostTooltip">Upload to Frost-Server</Translate>}>
                  <IconButton className={classes.space} style={{ color: '#fff' }} onClick={uploadToFisa} color="inherit">
                    <CloudUploadIcon fontSize="large" />
                  </IconButton>
                </Tooltip>
                <UploadToFrostDialog open={uploadDialogOpen} handleClose={() => setUploadDialogOpen(false)} />
              </div>
            )}
            {props.isAuthenticated && props.isAdmin && (
              <AdminMenu showSwagger={props.isSwaggerEnabled} showDatabase={!props.isInProduction} />
            )}
            <LocaleMenu currentLocale={props.currentLocale} onClick={handleLocaleChange} />
            <AccountMenu isAuthenticated={props.isAuthenticated} />
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
};

/*
Locale Menu not added jet.
<LocaleMenu currentLocale={props.currentLocale} onClick={handleLocaleChange} />
 */


const mapStateToProps = (state: IRootState) => ({
  currentLocale: state.locale.currentLocale,
  isAuthenticated: state.authentication.isAuthenticated,
  isAdmin: hasAnyAuthority(state.authentication.account.authorities, [AUTHORITIES.ADMIN]),
  ribbonEnv: state.applicationProfile.ribbonEnv,
  isInProduction: state.applicationProfile.inProduction,
  isSwaggerEnabled: state.applicationProfile.isSwaggerEnabled,
  canUndo: getCanUndo(state),
  canRedo: getCanRedo(state),
});

const mapDispatchToProps = {
  onLocaleChange: setLocale,
  undo,
  redo,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
