import React from 'react';
import { IconButton, Tooltip } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import { Translate } from 'react-jhipster';
import SaveAtFisa from './SaveAtFisa';

function SaveMenu() {
  const [saveAtFisaOpen, setSaveAtFisaOpen] = React.useState(false);

  return (
    <>
      <Tooltip title={<Translate contentKey="global.menu.saveMenu.saveProject">Save Project</Translate>}>
        <IconButton key="UploadMenu" style={{ color: '#fff', padding: '8px 10px' }} onClick={() => setSaveAtFisaOpen(true)} edge="start">
          <SaveIcon fontSize="large" />
        </IconButton>
      </Tooltip>
      <SaveAtFisa open={saveAtFisaOpen} onClose={() => setSaveAtFisaOpen(false)} />
    </>
  );
}

export default SaveMenu;
