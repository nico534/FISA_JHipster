import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import MuiAlert from '@material-ui/lab/Alert';
import { Dialog, DialogTitle, DialogContent, TextField, DialogActions, Button, Snackbar, CircularProgress } from '@material-ui/core';

import { updateProject, addProject, clearErrorMessage, resetExistsOnBackend, changeProjectName } from 'app/shared/actions';
import { FisaProjectI, ErrorMessageI } from 'app/shared/model';
import {
  getFisaProjectFromState,
  getServerCommunicationError,
  getServerCommunicationPending,
  getProjectExistsOnBackend,
} from 'app/shared/selectors';

import { IRootState } from 'app/shared/reducers';
import { Translate } from 'react-jhipster';
import ErrorSnackbar from '../../../modules/components/errorMessages/ErrorSnackbar';

interface SaveDialogProps {
  open: boolean;
  onClose: () => void;
  updateProject: (project: FisaProjectI, user: number) => void;
  addProject: (project: FisaProjectI, user: number) => void;
  fisaProject: FisaProjectI;
  saveError: ErrorMessageI | undefined;
  fetchPending: boolean;
  resetErrorMsg: () => void;
  resetOnBackend: () => void;
  existsOnBackend: boolean;
  changeName: (newName: string) => void;
  userId: number;
}

enum MenuStates {
  SHOW_NAME_MENU,
  PENDING,
  OVERRIDE_WARNING,
}

function SaveAtFisa(props: SaveDialogProps) {
  const [alertOpen, setAlertOpen] = React.useState(false);
  const [storeProjectName, setStoreProjectName] = React.useState(props.fisaProject.name);
  const [menuState, setMenuState] = React.useState(MenuStates.SHOW_NAME_MENU);

  const [tryToSave, setTryToSave] = React.useState(false);

  const tryToPutProject = () => {
    props.resetOnBackend();
    setMenuState(MenuStates.PENDING);
    props.addProject(props.fisaProject, props.userId);
    setTimeout(() => setTryToSave(true), 100);
  };

  const changeName = (name: string) => {
    props.changeName(name);
  };

  const closeMenu = () => {
    props.onClose();
    props.resetOnBackend();
    setTryToSave(false);
    setTimeout(() => setMenuState(MenuStates.SHOW_NAME_MENU), 3000);
  };

  const forceUpdateProject = () => {
    props.updateProject(props.fisaProject, props.userId);
    setMenuState(MenuStates.PENDING);
    setAlertOpen(true);
    closeMenu();
  };

  const { existsOnBackend, resetErrorMsg, onClose, fetchPending } = props;
  const projectName = props.fisaProject.name;

  useEffect(() => {
    if (tryToSave && !fetchPending) {
      if (existsOnBackend) {
        setMenuState(MenuStates.OVERRIDE_WARNING);
        setTryToSave(false);
        resetErrorMsg();
      } else {
        setStoreProjectName(projectName);
        onClose();
        setTryToSave(false);
        setTimeout(() => setMenuState(MenuStates.SHOW_NAME_MENU), 3000);
        setAlertOpen(true);
      }
    }
  }, [fetchPending, tryToSave, existsOnBackend, resetErrorMsg, onClose, projectName]);

  let dialogContent;

  switch (menuState) {
    case MenuStates.SHOW_NAME_MENU:
      dialogContent = (
        <>
          <DialogTitle><Translate contentKey="global.menu.saveAtFisa">Save at Fisa</Translate></DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              value={props.fisaProject.name}
              onChange={e => changeName(e.target.value)}
              margin="dense"
              id="name"
              label="Name"
              type="url"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => {
                props.onClose();
                props.changeName(storeProjectName);
              }}
            >
              <Translate contentKey="global.menu.cancel">Cancel</Translate>
            </Button>
            <Button disabled={props.fisaProject.name === ''} onClick={tryToPutProject}>
              <Translate contentKey="global.menu.save">Save</Translate>
            </Button>
          </DialogActions>
        </>
      );
      break;
    case MenuStates.OVERRIDE_WARNING:
      dialogContent = (
        <>
          <DialogTitle>
            <Translate contentKey="global.menu.saveMenu.overrideWarningTitle">Override Warning</Translate>
          </DialogTitle>
          <DialogContent>
            <Translate contentKey="global.menu.saveMenu.overrideWarningContent" interpolate={{ name: props.fisaProject.name }}>
              There already exists an object with the name: &quot;{props.fisaProject.name}&quot; on FISA
            </Translate>
          </DialogContent>
          <DialogContent>
            <Translate contentKey="global.menu.saveMenu.doYouWantToOverrideQuestion">Do you really want to override it?</Translate>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setMenuState(MenuStates.SHOW_NAME_MENU)}>
              <Translate contentKey="global.menu.no">No</Translate>
            </Button>
            <Button onClick={forceUpdateProject}>
              <Translate contentKey="global.menu.yes">Yes</Translate>
            </Button>
          </DialogActions>
        </>
      );
      break;
    case MenuStates.PENDING:
      dialogContent = (
        <DialogContent style={{ textAlign: 'center' }}>
          <CircularProgress />
        </DialogContent>
      );
      break;
    default:
      dialogContent = <div>{menuState} is not a valide State</div>;
  }

  return (
    <>
      <Dialog open={props.open}>{dialogContent}</Dialog>
      <Snackbar
        open={alertOpen && props.saveError === undefined && !props.fetchPending}
        autoHideDuration={6000}
        onClose={() => setAlertOpen(false)}
      >
        <MuiAlert elevation={6} variant="filled" onClose={() => setAlertOpen(false)} severity="success">
          <Translate contentKey="global.menu.saveMenu.success" interpolate={{ name: props.fisaProject.name }}>
            &quot;{props.fisaProject.name}&quot; successfully saved!
          </Translate>
        </MuiAlert>
      </Snackbar>
      <ErrorSnackbar
        open={alertOpen && props.saveError !== undefined}
        onClose={() => {
          setAlertOpen(false);
          props.resetErrorMsg();
        }}
        error={props.saveError}
      />
    </>
  );
}

const stateToSaveDialogProps = (state: IRootState) => ({
  fisaProject: getFisaProjectFromState(state, false, false),
  saveError: getServerCommunicationError(state),
  fetchPending: getServerCommunicationPending(state),
  existsOnBackend: getProjectExistsOnBackend(state),
  userId: state.authentication.account.id,
});

const dispatchToSaveDialogProps = {
  updateProject,
  addProject,
  resetErrorMsg: clearErrorMessage,
  resetOnBackend: resetExistsOnBackend,
  changeName: changeProjectName,
};

export default connect(stateToSaveDialogProps, dispatchToSaveDialogProps)(SaveAtFisa);
