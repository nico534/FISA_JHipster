import { SERVER_API_URL } from 'app/config/constants';

export const getLoginUrl = () => {
  const port = location.port ? `:${location.port}` : '';

  // If you have configured multiple OIDC providers, then, you can update this URL to /login.
  // It will show a Spring Security generated login page with links to configured OIDC providers.
  return `//${location.hostname}${port}${location.pathname}oauth2/authorization/oidc`;
};

export enum Routes {
  ROOT = '/',
  PROJECT = '/project',
  PAGE_NOT_FOUND = '/404',
}

export const BackendUrl = '';
