export interface AvailableFisaDocumentI {
  name: string;
  id: number;
  owner: boolean;
}

export interface AvailableProjectI {
  name: string;
  id: number;
}
