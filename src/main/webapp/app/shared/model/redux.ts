export interface ActionI {
  type: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  payload: any;
}

export interface DatastreamData {
  id: string | number;
  name: string;
}

export interface ErrorMessageI {
  name: string;
  message: string;
  code: number | undefined;
  longMessage: string | undefined;
  rawMessage: string | undefined;
}
