/* eslint-disable @typescript-eslint/no-explicit-any */
import { Dispatch } from 'redux';
import axios, { AxiosError } from 'axios';
import { OVERRIDE_ERROR_CODE } from 'app/variables/variables';
import * as actionTypes from '../actionTypes';
import { FisaProjectI, AvailableProjectI, AvailableFisaDocumentI, FisaDocumentI, ActionI, ErrorMessageI } from '../model';

import {
  clearRemovedObjects,
  loadSavedProject,
  setConnectedFrostUrl,
  setFromBackend,
  setFrostIdsOfObjects,
  setProjectId,
} from './projectActions';
import { setSaved, setDatastreamConnectionData } from './pageActions';

export const setCommunicationPending = () => ({
  type: actionTypes.COMMUNICATION_PENDING,
  payload: undefined,
});

const setCommunicationSuccess = () => ({
  type: actionTypes.COMMUNICATION_SUCCESS,
  payload: undefined,
});

export const setErrorToShow = (error: ErrorMessageI) => ({
  type: actionTypes.SET_ERROR_MESSAGE,
  payload: {
    error,
  },
});

export const clearErrorMessage = () => ({
  type: actionTypes.CLEAR_ERROR_MESSAGE,
  payload: undefined,
});

const stopCommunicationPending = () => ({
  type: actionTypes.STOP_COMMUNICATION_PENDING,
  payload: undefined,
});

function createErrorMessage(error: AxiosError<Error>): ErrorMessageI {
  const { message } = error;

  const { name } = error;

  const rMessage = (error.response && error.response.data.message) || (error.response && ((error.response.data as unknown) as string));

  return {
    name,
    message: rMessage || message,
    longMessage: rMessage !== undefined ? message : undefined,
    rawMessage: error.response ? JSON.stringify(error.response, null, 2) : undefined,
    code: error.request?.status,
  };
}

export const loadFromPC = (project: FisaProjectI) => (dispatch: Dispatch<ActionI>) => {
  // Check Document in backend
  dispatch(loadSavedProject(project));
};

export const loadProjectFromServer = (id: number) => (dispatch: Dispatch<ActionI>) => {
  dispatch(setCommunicationPending());
  return axios
    .get(`api/projects/${id}`)
    .then(response => {
      dispatch(loadSavedProject(response.data));
      dispatch(setCommunicationSuccess());
    })
    .catch(error => dispatch(setErrorToShow(createErrorMessage(error))))
    .finally(() => {
      dispatch(stopCommunicationPending());
    });
};

const setAvailableProjects = (availableProjects: AvailableProjectI[]) => ({
  type: actionTypes.ADD_PROJECTS_FROM_SERVER,
  payload: {
    availableProjects,
  },
});

export const fetchAvailableProjects = () => (dispatch: Dispatch<ActionI>) => {
  dispatch(setCommunicationPending());
  return axios
    .get('api/projects/')
    .then(response => {
      dispatch(setAvailableProjects(response.data));
      dispatch(setCommunicationSuccess());
    })
    .catch((e: AxiosError<Error>) => {
      dispatch(setErrorToShow(createErrorMessage(e)));
    })
    .finally(() => {
      dispatch(stopCommunicationPending());
    });
};

const setAvailableDocuments = (availableDocuments: AvailableFisaDocumentI[]) => ({
  type: actionTypes.ADD_DOCUMENTS_FROM_SERVER,
  payload: { availableDocuments },
});

export const fetchAvailableDocuments = () => (dispatch: Dispatch<ActionI>) => {
  dispatch(setCommunicationPending());
  return axios
    .get('api/documents/')
    .then(response => {
      dispatch(setAvailableDocuments(response.data));
      dispatch(setCommunicationSuccess());
    })
    .catch(e => {
      dispatch(setErrorToShow(createErrorMessage(e)));
    })
    .finally(() => {
      dispatch(stopCommunicationPending());
    });
};

export function deleteDocument(id: number) {
  return (dispatch: any) => {
    dispatch(setCommunicationPending());
    return axios
      .delete(`api/documents/${id}`)
      .then(() => {
        dispatch(fetchAvailableDocuments());
        dispatch(setCommunicationSuccess());
      })
      .catch(e => dispatch(setErrorToShow(createErrorMessage(e))))
      .finally(() => {
        dispatch(stopCommunicationPending());
      });
  };
}

export const addDocument = (document: FisaDocumentI, userId: number) => (dispatch: any) => {
  dispatch(setCommunicationPending());
  return axios
    .post(`api/documents/?userId=${userId}`, JSON.stringify(document), {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(() => {
      dispatch(fetchAvailableDocuments());
      dispatch(setCommunicationSuccess());
    })
    .catch(e => dispatch(setErrorToShow(createErrorMessage(e))))
    .finally(() => {
      dispatch(stopCommunicationPending());
    });
};

/**
 * Add a new Project to the FROST-Server
 * @param project the project to add
 * @param userId the id of the user
 */
export const addProject = (project: FisaProjectI, userId: number) => (dispatch: Dispatch<ActionI>) => {
  dispatch(setCommunicationPending());
  return axios
    .post(`api/projects/?userId=${userId}`, JSON.stringify(project), {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(data => {
      dispatch(setCommunicationSuccess());
      dispatch(setSaved());
      dispatch(setProjectId(data.data.id));
    })
    .catch(error => {
      if (createErrorMessage(error).code === OVERRIDE_ERROR_CODE) {
        dispatch(setFromBackend());
      } else {
        dispatch(setErrorToShow(createErrorMessage(error)));
      }
    })
    .finally(() => dispatch(stopCommunicationPending()));
};

export const updateProject = (project: FisaProjectI, userId: number) => (dispatch: Dispatch<ActionI>) => {
  dispatch(setCommunicationPending());
  return axios
    .put(`api/projects/?userId=${userId}`, JSON.stringify(project), {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(() => {
      dispatch(setCommunicationSuccess());
      dispatch(setSaved());
    })
    .catch(e => dispatch(setErrorToShow(createErrorMessage(e))))
    .finally(() => {
      dispatch(stopCommunicationPending());
    });
};

export const setChosenDocumentId = (id: number) => ({
  type: actionTypes.SET_CHOSEN_DOCUMENT_ID,
  payload: { id },
});

export const uploadProjectToFrost = (project: FisaProjectI, frostUrl: string) => (dispatch: Dispatch<ActionI>) => {
  dispatch(setCommunicationPending());
  const encodetUrl = encodeURIComponent(frostUrl);
  return axios
    .post(`api/projects/frostServer/upload/?url=${encodetUrl}`, JSON.stringify(project), {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      dispatch(setCommunicationSuccess());
      dispatch(setFrostIdsOfObjects(response.data.updatedObjects));
      dispatch(setDatastreamConnectionData(response.data.datastreamConnectionData));
      dispatch(setConnectedFrostUrl(frostUrl));
      dispatch(clearRemovedObjects());
    })
    .catch(error => {
      dispatch(setErrorToShow(createErrorMessage(error)));
    })
    .finally(() => {
      dispatch(stopCommunicationPending());
    });
};

export const updateProjectOnFrost = (project: FisaProjectI) => (dispatch: Dispatch<ActionI>) => {
  dispatch(setCommunicationPending());
  return axios
    .put('api/projects/frostServer/update/', JSON.stringify(project), {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      dispatch(setCommunicationSuccess());
      dispatch(setFrostIdsOfObjects(response.data.updatedObjects));
      dispatch(setDatastreamConnectionData(response.data.datastreamConnectionData));
      dispatch(clearRemovedObjects());
    })
    .catch(error => {
      dispatch(setErrorToShow(createErrorMessage(error)));
    })
    .finally(() => {
      dispatch(stopCommunicationPending());
    });
};

export const deleteProjectFromBackend = (id: number) => (dispatch: any) => {
  dispatch(setCommunicationPending());
  return axios
    .delete(`api/projects/${id}`)
    .then(() => {
      dispatch(setCommunicationSuccess());
      dispatch(fetchAvailableProjects());
    })
    .catch(e => dispatch(setErrorToShow(createErrorMessage(e))))
    .finally(() => dispatch(stopCommunicationPending()));
};

export const loadProjectFromFisa = (document: FisaDocumentI) => {
  return {
    type: actionTypes.LOAD_PROJECT_FROM_FISA,
    payload: {
      document,
    },
  };
};

/**
 * This function fetches the initial data from the server
 */
export const fetchProject = (documentId: number) => (dispatch: Dispatch<ActionI>) => {
  dispatch(setCommunicationPending());
  return axios
    .get(`api/documents/${documentId}`)
    .then(data => {
      dispatch(loadProjectFromFisa(data.data));
      dispatch(setCommunicationSuccess());
    })
    .catch(e => {
      dispatch(setErrorToShow(createErrorMessage(e)));
    })
    .finally(() => dispatch(stopCommunicationPending()));
};

export const deleteProjectFromBackendAndServer = (id: number) => (dispatch: any) => {
  dispatch(setCommunicationPending());
  return axios
    .delete(`api/projects/frostServer/${id}`)
    .then(() => {
      dispatch(setCommunicationSuccess());
      dispatch(fetchAvailableProjects());
    })
    .catch(e => {
      dispatch(setErrorToShow(createErrorMessage(e)));
    })
    .finally(() => dispatch(stopCommunicationPending()));
};
