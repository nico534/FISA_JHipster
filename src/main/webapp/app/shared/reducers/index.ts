import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import fisaProjectReducer, { ProjectStateI } from 'app/shared/reducers/fisaProjectReducer';
import projectPageReducer, { ProjectPageStateI } from 'app/shared/reducers/projectPageReducer';
import availableFisaDocumentsReducer, { DocumentProjectOverviewReducerI } from 'app/shared/reducers/overviewReducer';
import serverCommunicationReducer, { ServerCommunicationStateI } from 'app/shared/reducers/serverCommunicationReducer';
import userManagement, { UserManagementState } from './user-management';
import applicationProfile, { ApplicationProfileState } from './application-profile';
import authentication, { AuthenticationState } from './authentication';
import locale, { LocaleState } from './locale';

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
  readonly fisaProject: ProjectStateI;
  readonly projectPage: ProjectPageStateI;
  readonly availableFisaDocumentsProjects: DocumentProjectOverviewReducerI;
  readonly serverCommunication: ServerCommunicationStateI;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
  fisaProject: fisaProjectReducer,
  projectPage: projectPageReducer,
  availableFisaDocumentsProjects: availableFisaDocumentsReducer,
  serverCommunication: serverCommunicationReducer,
});

export default rootReducer;
