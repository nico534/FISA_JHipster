export const HOME = '/';
export const LOGOUT = `${HOME}logout`;
export const ADMIN = `${HOME}admin`;
export const PROJECT = `${HOME}project`;
