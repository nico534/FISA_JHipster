export const CONSTANT_PARTS = 'constantParts';
export const FISA_OBJECTS = 'fisaObjects';
export const LATEST_ID = 'latestId';
export const CONNECTED_FROST_URL = 'connectedFrostServerURL';

export const DEFAULT_FROST_URL = 'http://localhost:8081/FROST-Server/v1.1';

export const ERROR_MAPS_TO = 'ErrorMapsTo';
export const OVERRIDE_ERROR_CODE = 409;

export const LANGUAGES = ['en', 'de'];

export const CSV_ROW_SEPARATOR = '\n';
export const CSV_COL_SEPARATOR = ';';
