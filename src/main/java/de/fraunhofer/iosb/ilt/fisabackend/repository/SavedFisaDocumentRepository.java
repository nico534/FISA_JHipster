package de.fraunhofer.iosb.ilt.fisabackend.repository;

import de.fraunhofer.iosb.ilt.fisabackend.domain.SavedFisaDocument;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the SavedFisaDocument entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SavedFisaDocumentRepository extends JpaRepository<SavedFisaDocument, Long> {

    @Query("select savedFisaDocument from SavedFisaDocument savedFisaDocument where savedFisaDocument.user.login = ?#{principal.preferredUsername}")
    List<SavedFisaDocument> findByUserIsCurrentUser();

    @Query("select savedFisaDocument from SavedFisaDocument savedFisaDocument where not savedFisaDocument.user.login = ?#{principal.preferredUsername}")
    List<SavedFisaDocument> findByUserIsNotCurrentUser();
}
