package de.fraunhofer.iosb.ilt.fisabackend.model;

public class MapNameListElement {
    private String name;
    private long id;
    private boolean owner;

    /**
     * New ListElement with Name and UUID
     * @param name
     * @param id
     */
    public MapNameListElement(String name, long id) {
        this.name = name;
        this.id = id;
    }

    public MapNameListElement(String name, long id, boolean owner) {
        this.name = name;
        this.id = id;
        this.owner = owner;
    }

    /**
     * @return The name of the ListElement
     */
    public String getName() {
        return name;
    }

    /**
     * @return The UUID of the ListElement
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @return if the active user owns this Element
     */
    public boolean getOwner() {
        return owner;
    }
}
