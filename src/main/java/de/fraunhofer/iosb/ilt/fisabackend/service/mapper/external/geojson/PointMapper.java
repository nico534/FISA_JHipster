package de.fraunhofer.iosb.ilt.fisabackend.service.mapper.external.geojson;

import de.fraunhofer.iosb.ilt.fisabackend.service.mapper.Mapper;
import de.fraunhofer.iosb.ilt.fisabackend.service.mapper.sta.StaAttributeMapper;
import de.fraunhofer.iosb.ilt.fisabackend.service.tree.FisaTreeNode;
import de.fraunhofer.iosb.ilt.sta.model.Entity;
import de.fraunhofer.iosb.ilt.sta.model.EntityType;
import org.geojson.Point;

import java.util.List;
import java.util.function.BiConsumer;

public class PointMapper<T extends Entity<T>> extends StaAttributeMapper<T> {
    private final BiConsumer<T, Point> setter;

    protected PointMapper(EntityType entityType, String attributeName,
                          BiConsumer<T, Point> setter) {
        super(entityType, attributeName);
        this.setter = setter;
    }

    @Override
    public void apply(FisaTreeNode treeNode, String... arguments) {
        Entity<?> entity = treeNode.getContext(Entity.class);
        if (!entityTypeMatches(entity)) {
            return;
        }
        @SuppressWarnings("unchecked")
        T checkedEntity = (T) entity;
        Point point = deserialize(arguments[0]);
        setter.accept(checkedEntity, point);
    }

    @Override
    public Mapper resolve(String mapsTo) {
        return null;
    }

    private static Point deserialize(String polygonString) {
        String stripped = polygonString.substring(1, polygonString.length() - 1);
        List<String> splitPoint = GeoJsonMapperUtils.split(stripped);
        return new Point(GeoJsonMapperUtils.listToPoint(splitPoint));
    }
}
