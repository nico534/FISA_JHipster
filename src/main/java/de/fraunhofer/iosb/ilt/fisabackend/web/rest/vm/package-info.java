/**
 * View Models used by Spring MVC REST controllers.
 */
package de.fraunhofer.iosb.ilt.fisabackend.web.rest.vm;
