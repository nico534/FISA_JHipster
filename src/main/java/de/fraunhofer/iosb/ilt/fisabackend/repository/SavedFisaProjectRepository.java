package de.fraunhofer.iosb.ilt.fisabackend.repository;

import de.fraunhofer.iosb.ilt.fisabackend.domain.SavedFisaProject;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the SavedFisaProject entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SavedFisaProjectRepository extends JpaRepository<SavedFisaProject, Long> {

    @Query("select savedFisaProject from SavedFisaProject savedFisaProject where savedFisaProject.user.login = ?#{principal.preferredUsername}")
    List<SavedFisaProject> findByUserIsCurrentUser();
}
