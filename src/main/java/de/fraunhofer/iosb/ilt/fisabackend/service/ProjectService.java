package de.fraunhofer.iosb.ilt.fisabackend.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.iosb.ilt.fisabackend.domain.SavedFisaProject;
import de.fraunhofer.iosb.ilt.fisabackend.domain.User;
import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaProject;
import de.fraunhofer.iosb.ilt.fisabackend.repository.SavedFisaProjectRepository;
import de.fraunhofer.iosb.ilt.fisabackend.service.exception.BadRequestException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Project service manages all requested operations from the frontend of the application related to the
 * FisaProject.
 */
public class ProjectService {


    private final SavedFisaProjectRepository repo;


    /**
     * Instantiates a new Project service. Default path is a subdirectory called ProjectRepository located
     * inside the application dir
     *
     * @param repository path to repository
     */
    public ProjectService(SavedFisaProjectRepository repository) {
        this.repo = repository;
    }

    /**
     *
     * Will return a fisa project by it's uuid.
     *
     * @param id the id of the project
     * @return the fisa project matching the uuid
     * @throws IOException If ProjectRepository operation failed
     */
    public FisaProject getProject(long id) throws IOException {
        ObjectMapper om = new ObjectMapper();
        SavedFisaProject savedProject = repo.getOne(id);
        FisaProject project = om.readValue(savedProject.getData(), FisaProject.class);
        project.setId(savedProject.getId());
        return project;
    }

    /**
     *
     * @param id - id to check
     * @return - true if an project with the given Id exists
     */
    public boolean existsById(long id) {
        return repo.existsById(id);
    }

    /**
     *
     * @param name the name to check
     * @return true if there is a project with the given name in the user-Repo
     */
    public boolean exists(String name) {
        List<SavedFisaProject> allSavedProjects = repo.findByUserIsCurrentUser();
        for(SavedFisaProject project: allSavedProjects){
            if(project.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Will create a List of Projects currently available in Repository.
     *
     * @return key value pair of uuid and name of the FisaProject
     */
    public Map<Long, String> listProjects() {
        Map<Long, String> list = new HashMap<>();
        List<SavedFisaProject> documentList = repo.findByUserIsCurrentUser();

        for (SavedFisaProject project : documentList) {
            list.put(project.getId(), project.getName());
        }

        return list;
    }

    /**
     * Saves a FisaProject in the Project Repository.
     *
     * @param project The FisaProject that has to be stored
     * @throws IOException if ProjectRepository operation failed
     * @return - The id of the saved Project
     */
    public long createProject(FisaProject project, String userId) throws IOException {

        User user = new User();
        user.setId(userId);
        ObjectMapper objectMapper = new ObjectMapper();
        String data = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        SavedFisaProject fs = new SavedFisaProject().name(project.getName()).data(data).user(user);
        SavedFisaProject savedProject = repo.save(fs);
        return savedProject.getId();
    }

    /**
     * removes a FisaProject from the repository.
     *
     * @param id the uuid of the FisaProject that has to be removed
     */
    public void deleteProject(long id)  {
        repo.deleteById(id);
    }

    /**
     * updates a FisaProject at the repository.
     *
     * @param project new version of the project to be updated
     * @throws IOException if ProjectRepository operation failed
     */
    public long updateProject(FisaProject project, String userId) throws IOException, BadRequestException {
        User user = new User();
        user.setId(userId);

        long id = project.getId();
        FisaProject oldVersion = getProject(id);
        if(!oldVersion.getName().equals(project.getName())){
            throw new BadRequestException("Invalid id", "fisaDocument", "idnull");
        }

        ObjectMapper om = new ObjectMapper();

        SavedFisaProject toSave = new SavedFisaProject()
            .name(project.getName())
            .data(om.writerWithDefaultPrettyPrinter().writeValueAsString(project))
            .user(user);

        toSave.setId(project.getId());

        SavedFisaProject savedProject = repo.save(toSave);

        project.setId(savedProject.getId());

        return project.getId();
    }
}
