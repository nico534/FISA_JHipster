package de.fraunhofer.iosb.ilt.fisabackend.service.mapper.external.geojson;

import de.fraunhofer.iosb.ilt.fisabackend.service.mapper.Mapper;
import de.fraunhofer.iosb.ilt.fisabackend.service.mapper.sta.StaAttributeMapper;
import de.fraunhofer.iosb.ilt.fisabackend.service.tree.FisaTreeNode;
import de.fraunhofer.iosb.ilt.sta.model.Entity;
import de.fraunhofer.iosb.ilt.sta.model.EntityType;
import org.geojson.LngLatAlt;
import org.geojson.Polygon;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class PolygonMapper<T extends Entity<T>> extends StaAttributeMapper<T> {

    private final BiConsumer<T, Polygon> setter;

    protected PolygonMapper(EntityType entityType, String attributeName,
                            BiConsumer<T, Polygon> setter) {
        super(entityType, attributeName);
        this.setter = setter;
    }

    @Override
    public void apply(FisaTreeNode treeNode, String... arguments) {
        Entity<?> entity = treeNode.getContext(Entity.class);
        if (!entityTypeMatches(entity)) {
            return;
        }
        @SuppressWarnings("unchecked")
        T checkedEntity = (T) entity;
        Polygon polygon = deserialize(arguments[0]);
        setter.accept(checkedEntity, polygon);
    }

    @Override
    public Mapper resolve(String mapsTo) {
        return null;
    }

    private static Polygon deserialize(String polygonString) {
        String stripped = polygonString.substring(1, polygonString.length() - 1);
        List<LngLatAlt> points = new ArrayList<>();
        for (String input : GeoJsonMapperUtils.split(stripped)) {
            String strip = input.substring(1, input.length() - 1);
            List<String> split = GeoJsonMapperUtils.split(strip);
            LngLatAlt lngLatAlt = GeoJsonMapperUtils.listToPoint(split);
            points.add(lngLatAlt);
        }
        return new Polygon(points);
    }
}
