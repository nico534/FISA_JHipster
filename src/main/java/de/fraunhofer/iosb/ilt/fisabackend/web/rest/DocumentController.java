package de.fraunhofer.iosb.ilt.fisabackend.web.rest;

import de.fraunhofer.iosb.ilt.fisabackend.domain.User;
import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaDocument;
import de.fraunhofer.iosb.ilt.fisabackend.repository.SavedFisaDocumentRepository;
import de.fraunhofer.iosb.ilt.fisabackend.service.DocumentService;
import de.fraunhofer.iosb.ilt.fisabackend.service.exception.ClientRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * The controller managing frontend requests related to FisaDocuments.
 */
@RestController
@RequestMapping("/api/documents")
@Transactional
public class DocumentController {
    private DocumentService documentService;

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentController.class);

    /**
     * Instantiates a new Document controller.
     *
     * @param fisaDocumentRepository path to repository folder
     */
    public DocumentController(
        SavedFisaDocumentRepository fisaDocumentRepository
    ) {
        this.documentService = new DocumentService(fisaDocumentRepository);
    }

    /**
     * Endpoint for listing all available FisaDocuments on the server.
     *
     * @return All available FisaDocuments as a tuple of UUID and Name
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/")
    public ResponseEntity listFisaDocuments() {
        return ResponseEntity.ok(documentService.listFisaDocument());
    }

    /**
     * Endpoint for creating a FisaDocument
     *
     * @return Response of backend operation
     * @param document The FisaDocument that should be created
     */
    @CrossOrigin(origins = "*")
    @PostMapping("/")
    public ResponseEntity createFisaDocument(@RequestParam("userId") String userId, @RequestBody FisaDocument document) {
        LOGGER.debug("creating FisaDocument: " + document.getName());

        if(documentService.exists(document.getName())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("A Document with the name: " + document.getName() + " already exists");
        }

        try {
            User user = new User();
            user.setId(userId);
            documentService.createFisaDocument(document, user);
            return ResponseEntity.ok("stored successfully");
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    /**
     * Endpoint for fetching one specific FisaDocument
     *
     * @param id The ID of the requested FisaDocument
     *
     * @return The FisaDocument
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/{uuid}")
    public ResponseEntity getFisaDocument(@PathVariable(value = "uuid")Long id) {
        FisaDocument document;
        try {
            document = documentService.getFisaDocument(id);
            return ResponseEntity.ok(document);
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    /**
     * Endpoint for deleting the FisaDocument
     *
     * @return Response of backend operation
     * @param id The ID of the FisaDLOGGER.error("User not found");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new BadRequestAlertException("User-Id not found", "projects", "nouser"));ocument to delete
     */
    @CrossOrigin(origins = "*")
    @DeleteMapping("/{uuid}")
    public ResponseEntity deleteFisaDocument(@PathVariable(value = "uuid")Long id) {
        if(documentService.isNotOwner(id)) {
            LOGGER.error("This Document is not owned by the active user");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("This Document is not owned by the active user");
        }
         documentService.deleteFisaDocument(id);
         return ResponseEntity.ok("deleted successfully");
    }

    /**
     * Offers the given FisaDocument as a file
     *
     * @param id The uuid of the FisaDocument
     *
     * @return The FisaDocument as a file
     *
     * @throws IOException
     * @throws ClientRequestException
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}/download")
    public ResponseEntity download(
            @PathVariable(value = "id")long id
    ) {
        try {
            FisaDocument document = documentService.getFisaDocument(id);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add(
                "Content-Disposition",
                "attachment; filename=\"" + document.getName() + ".json\""
            );
            return ResponseEntity.ok()
                .headers(responseHeaders)
                .contentType(MediaType.APPLICATION_JSON)
                .body(document);
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(e.getMessage());
        }
      }

}
