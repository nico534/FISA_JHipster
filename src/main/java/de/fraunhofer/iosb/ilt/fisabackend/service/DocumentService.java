package de.fraunhofer.iosb.ilt.fisabackend.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.iosb.ilt.fisabackend.domain.SavedFisaDocument;
import de.fraunhofer.iosb.ilt.fisabackend.domain.User;
import de.fraunhofer.iosb.ilt.fisabackend.model.MapNameListElement;
import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaDocument;
import de.fraunhofer.iosb.ilt.fisabackend.repository.SavedFisaDocumentRepository;
import de.fraunhofer.iosb.ilt.fisabackend.service.exception.ClientRequestException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Document service manages all requested operations from the frontend of the application related to the
 * FisaDocument.
 */
public class DocumentService {

    private SavedFisaDocumentRepository repo;

    /**
     * Instantiates a new Document service. Default path is a subdirectory called DocumentRepository located
     * inside the application dir
     *
     * @param repository the repository
     */
    public DocumentService(SavedFisaDocumentRepository repository) {
        repo = repository;
    }

    /**
     *
     * Will return a fisa document by it's uuid.
     *
     * @param id the uuid generated from the use-case name
     * @return the fisa document matching the uuid
     * @throws IOException If DocumentRepository operation failed
     * @throws ClientRequestException if uuid cannot be found in repo
     */
    public FisaDocument getFisaDocument(long id) throws IOException {
        ObjectMapper om = new ObjectMapper();
        return om.readValue(repo.getOne(id).getData(), FisaDocument.class);
    }

    /**
     *
     * Will return a fisa document by it's uuid.
     *
     * @param uuid the uuid generated from the use-case name
     * @return the fisa document matching the uuid
     * @throws ClientRequestException if uuid cannot be found in repo

    public File getFisaDocumentAsFile(long id) throws ClientRequestException {

        return repo.getFisaDocumentAsFile(uuid);
    }
     */

    /**
     * Will create a List of use-cases currently available in Repository.
     *
     * @return key value pair of uuid and name of the FisaDocument
     */
    public List<MapNameListElement> listFisaDocument() {
        List<SavedFisaDocument> documentsOfUser = repo.findByUserIsCurrentUser();
        List<SavedFisaDocument> documentsNotOfUser = repo.findByUserIsNotCurrentUser();

        List <MapNameListElement> list = new ArrayList<>();

        for (SavedFisaDocument document : documentsOfUser) {
            list.add(new MapNameListElement(document.getName(), document.getId(), true) );
        }

        for (SavedFisaDocument document : documentsNotOfUser) {
            list.add(new MapNameListElement(document.getName(), document.getId(), false));
        }
        return list;
    }

    /**
     * Saves a FisaDocument in the Document Repository.
     *
     * @param doc The FisaDocument that has to be stored
     * @param user
     * @throws IOException if DocumentRepository operation failed
     */
    public void createFisaDocument(FisaDocument doc, User user) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String data = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(doc);
        SavedFisaDocument fs = new SavedFisaDocument().name(doc.getName()).data(data).user(user);
        repo.save(fs);
    }

    /**
     * removes a FisaDocument from the repository.
     *
     * @param id the uuid of the FisaDocument that has to be removed
     */
    public void deleteFisaDocument(long id) {
        repo.deleteById(id);
    }

    /**
     *
     * @param name the name to check
     * @return true if there is a project with the given name in the user-Repo
     */
    public boolean exists(String name) {
        List<SavedFisaDocument> allSavedProjects = repo.findAll();
        for(SavedFisaDocument project: allSavedProjects){
            if(project.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param id the Document-Id to check
     * @return true if there is a project with the given name in the user-Repo
     */
    public boolean isNotOwner(long id) {
        List<SavedFisaDocument> allSavedProjects = repo.findByUserIsCurrentUser();
        for(SavedFisaDocument project: allSavedProjects){
            if(project.getId() == id) {
                return false;
            }
        }
        return true;
    }
}
