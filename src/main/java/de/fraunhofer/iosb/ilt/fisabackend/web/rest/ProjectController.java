package de.fraunhofer.iosb.ilt.fisabackend.web.rest;

import de.fraunhofer.iosb.ilt.fisabackend.model.MapNameListElement;
import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaProject;
import de.fraunhofer.iosb.ilt.fisabackend.repository.SavedFisaProjectRepository;
import de.fraunhofer.iosb.ilt.fisabackend.service.FrostService;
import de.fraunhofer.iosb.ilt.fisabackend.service.ProjectService;
import de.fraunhofer.iosb.ilt.fisabackend.service.converter.FisaConverter;
import de.fraunhofer.iosb.ilt.fisabackend.service.exception.BadRequestException;
import de.fraunhofer.iosb.ilt.fisabackend.service.exception.EntityTransferException;
import de.fraunhofer.iosb.ilt.fisabackend.web.rest.errors.BadRequestAlertException;
import de.fraunhofer.iosb.ilt.sta.ServiceFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The controller managing frontend requests related to FisaProjects.
 */
@RestController
@RequestMapping("/api/projects")
@Transactional
public class ProjectController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

    private final ProjectService projectService;
    private final FrostService frostService;

    /**
     * Instantiates a new Document controller.
     *
     * @param repository path to repository
     * @param fisaConverter converter for FisaProject -> OGCSensorThingsApi conversion
     */
    public ProjectController(
            SavedFisaProjectRepository repository,
            FisaConverter fisaConverter) {
        this.projectService = new ProjectService(repository);
        this.frostService = new FrostService(fisaConverter);
    }

    /**
     * Takes uuid from frontend and retrieves the matching FisaProject from backend.
     *
     * @param id id matching the requested FisaProject
     * @return the requested FisaProject
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/{projectUuid}")
    public ResponseEntity getProject(@PathVariable(value = "projectUuid")long id) {
        try {
            FisaProject project = projectService.getProject(id);
            return ResponseEntity.ok(project);
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(e.getMessage());
        }
    }

    /**
     * Creates a map of pairs of uuid and project names lying in the backend repository.
     *
     * @return the pairs of uuid and project name
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/")
    public ResponseEntity listFisaProjects() {
        LOGGER.info("Get List of FISA-Projects");

        List<MapNameListElement> documents = new ArrayList<>();

        projectService.listProjects().forEach((uuid, name) -> documents.add(new MapNameListElement(name, uuid)));
        return ResponseEntity.ok(documents);
    }

    /**
     * Takes FisaProject from Frontend and stores it onto backend.
     *
     * @return Response to backend operation
     * @param project The FisaProject that will be stored in repository
     */
    @CrossOrigin(origins = "*")
    @PostMapping("/")
    public ResponseEntity createProject(@RequestParam("userId") String userId, @RequestBody FisaProject project) {
        LOGGER.debug("Create project...");

        if(projectService.exists(project.getName())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("A project with the name: " + project.getName() + " already exists");
        }

        try {
            long newId  = projectService.createProject(project, userId);
            Map<String, String> response = new HashMap<>();
            response.put("id", Long.toString(newId));
            return ResponseEntity.ok(response);
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    /**
     * Takes a newer version of a FisaProject from frontend and updates the stored one on the backend.
     *
     * @return Response of backend operation
     * @param project the project that has to be updated
     */
    @CrossOrigin(origins = "*")
    @PutMapping("/")
    public ResponseEntity updateProject(@RequestParam("userId") String userId, @RequestBody FisaProject project) {
        try {
            projectService.updateProject(project, userId);
            return ResponseEntity.ok("updated successfully");
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        } catch (BadRequestException e) {
            throw new BadRequestAlertException(e.getMessage(), e.getEntityName(), e.getErrorKey());
        }
    }

    /**
     * Deletes FisaProject from backend.
     *
     * @return Response of backend operation
     * @param id the uuid of the FisaProject that has to be deleted
     */
    @CrossOrigin(origins = "*")
    @DeleteMapping("/{projectUuid}")
    public ResponseEntity deleteFisaDocument(@PathVariable(value = "projectUuid") long id) {
        projectService.deleteProject(id);
        return ResponseEntity.ok("deleted successfully");

    }

    /**
     * Sends FisaProject to Frost Server
     *
     * @param url     the url of the target server
     * @param project the project that has to be converted and uploaded
     * @return Response of the backend operation
     */
    @CrossOrigin(origins = "*")
    @PostMapping("/frostServer/upload")
    public ResponseEntity sendToFrost(@RequestParam("url") String url, @RequestBody FisaProject project) {
        LOGGER.info("Received project, uploading to: " + url + "...");
        try {
            return ResponseEntity.ok(this.frostService.sendToFrost(url, project));
        } catch (EntityTransferException e) {
            LOGGER.error("Communication with SensorThingsApi-Server failed", e);
            return ResponseEntity.status(e.getStatusCode())
                    .body(e.getReturnedContent());
        } catch (ServiceFailureException e) {
            LOGGER.error("Communication with SensorThingsApi-Server failed", e);
            return ResponseEntity.status(HttpStatus.BAD_GATEWAY)
                    .body("Communication with SensorThingsApi-Server failed: " + e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to fulfill request: " + e.getMessage());
        }
    }

    /**
     * Updates an existing Project on a FROST-Server
     *
     * @param project the project that has to be converted and uploaded
     * @return Response of the backend operation
     */
    @CrossOrigin(origins = "*")
    @PutMapping("/frostServer/update")
    public ResponseEntity updateFrostServer(@RequestBody FisaProject project) {
        LOGGER.info("Received project, updating...");
        try {
            return ResponseEntity.ok(this.frostService.updateFrostServer(project));
        } catch (EntityTransferException e) {
            LOGGER.error("Communication with SensorThingsApi-Server failed", e);
            return ResponseEntity.status(e.getStatusCode())
                .body(e.getReturnedContent());
        } catch (ServiceFailureException e) {
            LOGGER.error("Communication with SensorThingsApi-Server failed", e);
            return ResponseEntity.status(HttpStatus.BAD_GATEWAY)
                .body("Communication with SensorThingsApi-Server failed: " + e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Failed to fulfill request: " + e.getMessage());
        }
    }

    /**
     * Removes the given Fisa-Project saved under the uuid from the FROST-Server
     *
     * @param id the uuid of the project to remove
     * @return Response of the backend operation
     */
    @CrossOrigin(origins = "*")
    @DeleteMapping("/frostServer/{projectUuid}")
    public ResponseEntity removeFromFrostAndBackend(@PathVariable(value = "projectUuid")long id) {
        LOGGER.info("Received project, removing from FROST-Server...");
        try {
            FisaProject project = projectService.getProject(id);
            this.frostService.removeProjectFromFrostServer(project);
            projectService.deleteProject(id);
            return ResponseEntity.ok("Successfully removed");
        } catch (MalformedURLException e) {
            LOGGER.error("Communication with SensorThingsApi-Server failed", e);
            return ResponseEntity.status(HttpStatus.BAD_GATEWAY)
                .body("Communication with SensorThingsApi-Server failed: " + e.getMessage());
        } catch (Exception e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Failed to fulfill request: " + e.getMessage());
        }
    }

    /**
     * Offers the project as a File
     *
     * @param  id uuid of the project
     *
     * @return The project
     *
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}/download")
    public ResponseEntity download(
            @PathVariable(value = "id")long id
    )  {
        try {
            FisaProject projectToDownload = projectService.getProject(id);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add(
                "Content-Disposition",
                "attachment; filename=\"" + projectToDownload.getName() + ".json\""
            );
            return ResponseEntity.ok()
                .headers(responseHeaders)
                .contentType(MediaType.APPLICATION_JSON)
                .body(projectToDownload);
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(e.getMessage());
        }
    }

    /* TODO
    @GetMapping()
    Map<String,UUID> getFrostProjectList(String url) {return null;}

    @GetMapping()
    FisaProject loadFrostProject(String url, UUID uuid) {return null;}
    */


}
