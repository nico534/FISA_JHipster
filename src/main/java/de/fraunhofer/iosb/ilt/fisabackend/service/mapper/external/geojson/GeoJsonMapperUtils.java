package de.fraunhofer.iosb.ilt.fisabackend.service.mapper.external.geojson;

import org.geojson.LngLatAlt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeoJsonMapperUtils {

    public static LngLatAlt listToPoint(List<String> rawPoint) {
        if (rawPoint.size() < 2) {
            throw new IllegalArgumentException("Point must have longitude and latitude: " + rawPoint);
        }
        double lng = Double.parseDouble(rawPoint.get(0));
        double lat = Double.parseDouble(rawPoint.get(1));
        double alt = 0;

        if (rawPoint.size() > 2) {
            alt = Double.parseDouble(rawPoint.get(2)); // we're just ignoring higher dimensions
        }

        return new LngLatAlt(lng, lat, alt);
    }

    public static List<String> split(String input) {
        if (input.indexOf('[') == -1 && input.indexOf(']') == -1) {
            return Arrays.asList(input.split(String.valueOf(',')));
        }
        int level = 0;
        int begin = 0;
        List<String> split = new ArrayList<>();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == ',' && level == 0) {
                split.add(input.substring(begin, i));
                begin = i + 1;
            } else if (c == '[') {
                level++;
            } else if (c == ']') {
                level--;
            }
        }
        if (begin < input.length()) {
            split.add(input.substring(begin));
        }
        return split;
    }
}
