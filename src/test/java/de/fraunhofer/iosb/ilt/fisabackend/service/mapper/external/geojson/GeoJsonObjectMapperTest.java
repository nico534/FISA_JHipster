package de.fraunhofer.iosb.ilt.fisabackend.service.mapper.external.geojson;

import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaObject;
import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaObjectAttribute;
import de.fraunhofer.iosb.ilt.fisabackend.service.mapper.Mapper;
import de.fraunhofer.iosb.ilt.fisabackend.service.mapper.external.geojson.GeoJsonObjectMapper;
import de.fraunhofer.iosb.ilt.fisabackend.service.mapper.external.geojson.PointMapper;
import de.fraunhofer.iosb.ilt.fisabackend.service.tree.FisaTreeNode;
import de.fraunhofer.iosb.ilt.sta.model.Entity;
import de.fraunhofer.iosb.ilt.sta.model.EntityType;
import de.fraunhofer.iosb.ilt.sta.model.Location;
import org.geojson.GeoJsonObject;
import org.geojson.Point;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GeoJsonObjectMapperTest {

    @Test
    void testApplyingPoint() {
        PointMapper<Location> pointMapper = new PointMapper<>(EntityType.LOCATION, "point", Location::setLocation);
        String pointValue = "[1,2]";
        FisaObjectAttribute attribute = new FisaObjectAttribute("point", pointValue);
        FisaObject fisaObject = new FisaObject(1, "location", new ArrayList<>(), List.of(attribute));
        FisaTreeNode node = new FisaTreeNode(null, fisaObject);
        Entity<Location> location = new Location();
        node.addContext(Entity.class, location);
        pointMapper.apply(node, pointValue);
        Location l = (Location) location;
        assertEquals(l.getLocation(), new Point(1, 2, 0));
    }

}
