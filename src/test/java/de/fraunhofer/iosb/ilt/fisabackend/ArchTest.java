package de.fraunhofer.iosb.ilt.fisabackend;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("de.fraunhofer.iosb.ilt.fisabackend");

        noClasses()
            .that()
                .resideInAnyPackage("de.fraunhofer.iosb.ilt.fisabackend.service..")
            .or()
                .resideInAnyPackage("de.fraunhofer.iosb.ilt.fisabackend.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..de.fraunhofer.iosb.ilt.fisabackend.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
