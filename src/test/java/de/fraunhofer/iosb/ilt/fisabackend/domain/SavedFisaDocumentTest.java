package de.fraunhofer.iosb.ilt.fisabackend.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import de.fraunhofer.iosb.ilt.fisabackend.web.rest.TestUtil;

public class SavedFisaDocumentTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SavedFisaDocument.class);
        SavedFisaDocument savedFisaDocument1 = new SavedFisaDocument();
        savedFisaDocument1.setId(1L);
        SavedFisaDocument savedFisaDocument2 = new SavedFisaDocument();
        savedFisaDocument2.setId(savedFisaDocument1.getId());
        assertThat(savedFisaDocument1).isEqualTo(savedFisaDocument2);
        savedFisaDocument2.setId(2L);
        assertThat(savedFisaDocument1).isNotEqualTo(savedFisaDocument2);
        savedFisaDocument1.setId(null);
        assertThat(savedFisaDocument1).isNotEqualTo(savedFisaDocument2);
    }
}
