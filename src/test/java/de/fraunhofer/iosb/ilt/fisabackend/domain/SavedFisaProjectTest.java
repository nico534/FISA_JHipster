package de.fraunhofer.iosb.ilt.fisabackend.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import de.fraunhofer.iosb.ilt.fisabackend.web.rest.TestUtil;

public class SavedFisaProjectTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SavedFisaProject.class);
        SavedFisaProject savedFisaProject1 = new SavedFisaProject();
        savedFisaProject1.setId(1L);
        SavedFisaProject savedFisaProject2 = new SavedFisaProject();
        savedFisaProject2.setId(savedFisaProject1.getId());
        assertThat(savedFisaProject1).isEqualTo(savedFisaProject2);
        savedFisaProject2.setId(2L);
        assertThat(savedFisaProject1).isNotEqualTo(savedFisaProject2);
        savedFisaProject1.setId(null);
        assertThat(savedFisaProject1).isNotEqualTo(savedFisaProject2);
    }
}
