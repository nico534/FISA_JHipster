import * as selectors from 'app/shared/selectors';
import { testState } from '../mockups/testState';

test('getServerCommunicationActive', () => {
  const expected = testState().serverCommunication.active;
  // @ts-ignore
  expect(selectors.getServerCommunicationActive(testState())).toBe(expected);
});

test('getServerCommunicationPending', () => {
  const expected = testState().serverCommunication.pending;
  // @ts-ignore
  expect(selectors.getServerCommunicationPending(testState())).toBe(expected);
});

test('getServerCommunicationError', () => {
  const expected = testState().serverCommunication.error;
  // @ts-ignore
  expect(selectors.getServerCommunicationError(testState())).toEqual(expected);
});

test('getProjectsFetched', () => {
  const expected = testState().availableFisaDocumentsProjects.projectsFetched;
  // @ts-ignore
  expect(selectors.getProjectsFetched(testState())).toBe(expected);
});

test('getDocumentsFetched', () => {
  const expected = testState().availableFisaDocumentsProjects.documentsFetched;
  // @ts-ignore
  expect(selectors.getDocumentsFetched(testState())).toBe(expected);
});
