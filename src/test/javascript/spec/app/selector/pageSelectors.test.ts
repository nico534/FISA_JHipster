import * as selectors from 'app/shared/selectors/pageSelectors';
import { testState } from '../mockups/testState';

test('getDontAskOnObjectDelete', () => {
  // @ts-ignore
  expect(selectors.getDontAskOnObjectDelete(testState())).toBeFalsy();
});

test('getNotSaved', () => {
  // @ts-ignore
  expect(selectors.getNotSaved(testState())).toBeFalsy();
});

describe('getCanUndo', () => {
  it('test canUndo with empty redoHistory', () => {
    // @ts-ignore
    expect(selectors.getCanRedo(testState())).toBeFalsy();
  });
  it('test canUndo with not empty redoHistory', () => {
    const modifiedState = {
      ...testState(),
      fisaProject: {
        ...testState().fisaProject,
        undoHistory: [
          {
            activeObject: 3,
            objects: testState().fisaProject.objects,
          },
        ],
      },
    };
    // @ts-ignore
    expect(selectors.getCanUndo(modifiedState)).toBeTruthy();
  });
});

describe('getCanRedo', () => {
  it('test canUndo with empty redoHistory', () => {
    // @ts-ignore
    expect(selectors.getCanRedo(testState())).toBeFalsy();
  });
  it('test canUndo with not empty redoHistory', () => {
    const modifiedState = {
      ...testState(),
      fisaProject: {
        ...testState().fisaProject,
        redoHistory: [
          {
            activeObject: 3,
            objects: testState().fisaProject.objects,
          },
        ],
      },
    };
    // @ts-ignore
    expect(selectors.getCanRedo(modifiedState)).toBeTruthy();
  });
});
