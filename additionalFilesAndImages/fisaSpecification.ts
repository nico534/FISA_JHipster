interface FisaDocument {
  name: string;
  objectDefinitions: FisaObjectDefinition[];
  fisaTemplate: FisaObject[];
}

interface FisaObjectDefinition  {
  name: string;
  caption: string | undefined;
  infoText: string | undefined;
  isTopLayer: boolean | undefined;
  mapsTo: string;
  exampleData: ExampleData | undefined;
  attributes: FisaObjectAttributeDefinition[];
  children: ChildDefinition[];
  isNotReusable: boolean | undefined;
}

export interface FisaObject {
  id: number;
  definitionName: string;
  children: number[];
  attributes: FisaObjectAttribute[];
}

interface ExampleData {
  count: number;
  valueMax: number;
  valueMin: number;
  timeMin: string;
  timeMax: string;
}

interface FisaObjectAttributeDefinition  {
  name: string;
  infoText: string | undefined;
  valueType: FisaAttributeValueType;
  value: string | undefined;
  isPredefined: boolean | undefined;
  dropDownValues: string[] | undefined;
  mapsTo: string;
  validationRule: string | undefined;
}

interface FisaObjectAttribute {
  definitionName: string;
  value: string;
}

// Possible types for value in there respective String-Representation:
type ValueType =
  | boolean
  | number
  | string
  | [number, number] // Point-Position
  | [number, number][]; // PolygonPosition

type FisaAttributeValueType =
  | 'string'
  | 'number'
  | 'boolean'
  | 'dropdown'
  | 'exampleDropdown'
  | 'pointPosition'
  | 'polygonPosition';

interface ChildDefinition {
  objectName: string;
  quantity: number;
}
